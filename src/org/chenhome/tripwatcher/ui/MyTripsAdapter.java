package org.chenhome.tripwatcher.ui;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.chenhome.tripwatcher.R;
import org.chenhome.tripwatcher.TwApp;
import org.chenhome.tripwatcher.data.Trip;
import org.chenhome.tripwatcher.data.TripMgr;
import org.chenhome.tripwatcher.ui.Util.Font;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MyTripsAdapter extends ArrayAdapter<Trip> {

	@SuppressLint("SimpleDateFormat")
	private static final SimpleDateFormat sF = new SimpleDateFormat(
			"MMM dd");

	private static final String TAG = "MyTripAdap";
	
	private LayoutInflater mInflater;
	private IataImgCache mCache;
	private CityCache mCityCache;

	private Activity mActivity;

	public MyTripsAdapter(Activity context) {
		super(context, -1);
		mActivity = context;
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mCache = TwApp.getAirportCache(context);
		mCityCache = TwApp.getCityCache(context);
	}

	public void setData(List<Trip> data) {
		clear();
		if (data != null) {
			addAll(data);
		}
	}

	/**
	 * Populate new items in the list.
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (convertView == null) {
			view = mInflater.inflate(R.layout.trip_card, parent, false);
		}

		final Trip item = getItem(position);
		String dstName = mCityCache.getCity(item.dst);
		if (dstName == null) {
			// Will later asynchronously update the adapter
			dstName = item.dst;
		}
		String orgName = mCityCache.getCity(item.org);
		if (orgName == null) {
			// Will later asynchronously update the adapter
			orgName = item.dst;
		}
		String title = getContext().getString(R.string.title_origin_dest, orgName, dstName);
		SpannableString cityStyle = Util.withCustomTypeface(getContext(),
				title, Font.BEBAS);
		((TextView) view.findViewById(R.id.trip_item_city)).setText(cityStyle);

		String dateCriteria = getContext().getString(R.string.date_all);
		if (item.isWeekend) {
			dateCriteria = getContext().getString(R.string.date_weekends);
		} else if (item.months != null && item.months.length > 0) {
			StringBuffer b =new StringBuffer();
			for (String month:item.months) {
				b.append(month);
				b.append(" ");
			}
			dateCriteria = getContext().getString(R.string.date_months, b.toString());
		} else if (item.getDeparting() != null || item.getReturning() != null) {
			Date depart = item.getDeparting();
			Date returning = item.getReturning();
			dateCriteria = getContext().getString(R.string.date_specific, sF.format(depart), sF.format(returning));
		}
		
		Util.setText(view, R.id.trip_item_date, dateCriteria);
		String price = getContext().getString(R.string.flt_price_known,
				(int) item.price);
		Util.setText(view, R.id.trip_item_price, price);
		

		ImageView imgView = (ImageView) view.findViewById(R.id.trip_item_img);
		String dstCode = IataImgCache.normalize(item.dst);
		Bitmap img = mCache.getIataImage(dstCode);
		if (img != null) {
			imgView.setImageBitmap(img);
		} else {
			imgView.setImageResource(R.drawable.loc_unknown);
		}
		
		view.findViewById(R.id.trip_item_delete).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				handleOnDelete(item); 
			}

		});
		

		view.findViewById(R.id.trip_item_share).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				handleOnShare(item);
			}

		});
		
		TextView delta = (TextView)view.findViewById(R.id.trip_delta);
		setDeltaText(delta, item);
		return view;
	}
	

	private void setDeltaText(TextView delta, Trip item) {
		if (item.priceDelta == 0) {
			delta.setVisibility(View.GONE);
			return;
		}
		delta.setVisibility(View.VISIBLE);
		int percent = (int) (100 * item.priceDelta);
		String val = Math.abs(percent) + "%";
		if (percent > 0) {
			// price drop
			val += " drop";
		} else {
			val += " up";
		}
		delta.setText(val);
	}

	private void handleOnDelete(final Trip item) {
		AsyncTask.execute(new Runnable() {
			@Override
			public void run() {
				TripMgr m =new TripMgr(getContext(), item);
				m.stopWatchingTrip(getContext());
				mActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						remove(item);
						notifyDataSetChanged();
					}
				});
			}
		});
	}

	private void handleOnShare(final Trip item) {
		mActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Trip.startShareChooser(item, mActivity);				
			}
		});
	}
}

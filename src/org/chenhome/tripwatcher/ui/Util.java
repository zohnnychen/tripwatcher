package org.chenhome.tripwatcher.ui;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

public class Util {

	public enum Font {
		QUICKSAND("fonts/Quicksand-Regular.otf"),
		BEBAS("fonts/BebasNeue.otf");

		// lives in /assets/fonts
		public String assetPath;
		private Font(String assetPath) {
			this.assetPath = assetPath;
		}
	}
	
	private static Map<String, Typeface> sTypeFaces = new HashMap<String, Typeface>();

	public static SpannableString withCustomTypeface(Context context, String src, Font typeface) {
		SpannableString s = new SpannableString(src);
		s.setSpan(new TypefaceSpan(context, typeface.assetPath), 0, s.length(),
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		return s;
	}

	public static void setTypeface(Context context, TextView view, Font typeface) {
		Typeface tf = getCustomTypeface(context, typeface.assetPath);
		view.setTypeface(tf);
		// Make it anti-aliased
		view.setPaintFlags(view.getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);

	}
	

	public static void setText(View parent, int id, String str) {
		try {
			((TextView) parent.findViewById(id)).setText(str);
		} catch (Exception e) {
		}
	}

	
	/**
	 * @param path
	 *            Path relative to the "<project>/assets/" folder
	 * @return
	 */
	private static Typeface getCustomTypeface(Context context, String path) {
		Typeface face = sTypeFaces.get(path);
		if (face == null) {
			face = Typeface.createFromAsset(context.getAssets(), path);
			sTypeFaces.put(path, face);
		}
		return face;
	}
	

	public static String getCity(String code, CityCache cityCache) {
		if (TextUtils.isEmpty(code)) {
			return null;
		}
		String val = cityCache.getCity(code);
		return (val == null) ? code : val;
	}

}

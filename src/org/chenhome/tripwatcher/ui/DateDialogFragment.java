package org.chenhome.tripwatcher.ui;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.chenhome.tripwatcher.FlightsActivity;
import org.chenhome.tripwatcher.R;
import org.chenhome.tripwatcher.data.Trip.Month;
import org.chenhome.tripwatcher.data.TripMgr;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.TextView;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
public class DateDialogFragment extends DialogFragment {

	private static final String TAG = "DateFrag";
	private TripMgr mMgr;
	private Type mType;

	public enum Type {
		BY_MONTH(R.string.by_month), BY_EXACT(R.string.by_exact);
		private Type(int strRes) {
			this.strRes = strRes;
		}

		public int strRes;
	}

	public static DateDialogFragment newInstance(Type type) {
		DateDialogFragment f = new DateDialogFragment();
		Bundle args = new Bundle();
		args.putString("type", type.name());
		f.setArguments(args);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Let exception be thrown
		Bundle args = getArguments();
		if (args != null && args.containsKey("type")
				&& !TextUtils.isEmpty(args.getString("type"))) {
			mType = Type.valueOf(args.getString("type"));
		}
		mMgr = ((FlightsActivity) getActivity()).getTripMgr();
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder b = new AlertDialog.Builder(getActivity()).setIcon(
				R.drawable.ic_action_opt_date).setTitle(mType.strRes);

		if (mType == Type.BY_MONTH) {
			initBuilderMonthDialg(b);
		} else if (mType == Type.BY_EXACT) {
			initBuilderExactDialog(b);
		}
		return b.show();
	}

	private void initBuilderExactDialog(Builder b) {
		View view = getActivity().getLayoutInflater().inflate(
				R.layout.dialog_byexact, null);

		final DatePicker departing = (DatePicker) view
				.findViewById(R.id.picker_departing);
		final DatePicker returning = (DatePicker) view
				.findViewById(R.id.picker_returning);

		final TextView departTitle = (TextView) view
				.findViewById(R.id.picker_departing_title);
		final TextView returnTitle = (TextView) view
				.findViewById(R.id.picker_returning_title);

		Calendar c = Calendar.getInstance();
		departing.setMinDate(c.getTimeInMillis() - 100000);
		returning.setMinDate(c.getTimeInMillis() - 100000);
		setTitleDayOfWeek(departTitle, c, R.string.departing);

		c.roll(Calendar.YEAR, true);
		departing.setMaxDate(c.getTimeInMillis());
		returning.setMaxDate(c.getTimeInMillis());
		setTitleDayOfWeek(returnTitle, c, R.string.returning);

		Calendar now = Calendar.getInstance();
		departing.init(now.get(Calendar.YEAR), now.get(Calendar.MONTH),
				now.get(Calendar.DAY_OF_MONTH), new OnDateChangedListener() {
					@Override
					public void onDateChanged(DatePicker view, int year,
							int monthOfYear, int dayOfMonth) {
						setTitleDayOfWeek(departTitle, year, monthOfYear,
								dayOfMonth, R.string.departing);
					}
				});

		now.roll(Calendar.DAY_OF_YEAR, 4); // default to 4 days later
		returning.init(now.get(Calendar.YEAR), now.get(Calendar.MONTH),
				now.get(Calendar.DAY_OF_MONTH), new OnDateChangedListener() {
					@Override
					public void onDateChanged(DatePicker view, int year,
							int monthOfYear, int dayOfMonth) {
						setTitleDayOfWeek(returnTitle, year, monthOfYear,
								dayOfMonth, R.string.returning);
					}
				});

		b.setView(view);

		final CheckBox isFlexible = (CheckBox) view
				.findViewById(R.id.picker_isflexible);
		b.setPositiveButton(android.R.string.ok, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Date depart = toDate(departing);
				Date returnDate = toDate(returning);
				if (depart != null && returnDate != null) {

					if (returnDate.before(depart)) {
						// Auto-adjust returning to something in the future
						Calendar cal = Calendar.getInstance();
						cal.setTime(depart);
						cal.roll(Calendar.DAY_OF_YEAR, 7);
						returnDate = cal.getTime();
						Log.d(TAG,
								"Auto adjusted return date to "
										+ returnDate.toLocaleString());
					}
					Log.d(TAG,
							"Setting to depart " + depart + ", return "
									+ returnDate + ", isFlexible? "
									+ isFlexible.isChecked());
					mMgr.setByDates(depart, returnDate, isFlexible.isChecked());
				}
			}
		});
	}

	private Date toDate(DatePicker picker) {
		Calendar c = Calendar.getInstance();
		c.set(picker.getYear(), picker.getMonth(), picker.getDayOfMonth());
		return c.getTime();
	}

	private void setTitleDayOfWeek(TextView returnTitle, Calendar c, int strRes) {
		setTitleDayOfWeek(returnTitle, c.get(Calendar.YEAR),
				c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), strRes);
	}

	private void setTitleDayOfWeek(final TextView departTitle, int year,
			int monthOfYear, int dayOfMonth, int strRes) {
		try {
			Calendar cal = Calendar.getInstance();
			cal.set(year, monthOfYear, dayOfMonth);
			String day = cal.getDisplayName(Calendar.DAY_OF_WEEK,
					Calendar.LONG, Locale.getDefault());
			departTitle.setText(getString(strRes, day));
		} catch (Exception e) {
			Log.w(TAG, "Unable to set new date", e);
		}
	}

	private void initBuilderMonthDialg(Builder b) {
		b.setItems(R.array.months, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Month selected = Month.fromOrdinal(which);
				if (mMgr != null && selected != null) {
					List<Month> inMonths = new ArrayList<Month>();
					inMonths.add(selected);
					mMgr.setByMonth(inMonths);
				}
			}
		});
	}

}

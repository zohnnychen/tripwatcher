package org.chenhome.tripwatcher.ui;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;
import android.util.LruCache;

@SuppressLint("DefaultLocale")
public class IataImgCache {
	private static final int BITMAP_WID = 320;
	private static final int BITMAP_HEIGHT = 160;
	private static final String TAG = "ThbCache";
	private static final String IATA_PATH = "iata_imgs";
	private LruCache<String, Bitmap> mMemoryCache;
	private Context mContext;
	private Map<String, String> mCode2Fname;

	public IataImgCache(final Context context) {
		mContext = context;
		// Get max available VM memory, exceeding this amount will throw an
		// OutOfMemory exception. Stored in kilobytes as LruCache takes an
		// int in its constructor.
		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

		// Use 1/8th of the available memory for this memory cache.
		final int cacheSize = maxMemory / 8;

		mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
			@Override
			protected int sizeOf(String key, Bitmap bitmap) {
				// The cache size will be measured in kilobytes rather than
				// number of items.
				return bitmap.getByteCount() / 1024;
			}
		};

		// Get list of iata asset names
		mCode2Fname = new HashMap<String, String>();
		try {
			String[] names = context.getAssets().list(IATA_PATH);
			for (String name : names) {
				String iataCode = getCode(name);
				if (!TextUtils.isEmpty(iataCode) && !TextUtils.isEmpty(name)) {
					mCode2Fname.put(iataCode, name);
				}
			}
			Log.d(TAG, "Loaded " + mCode2Fname.size() + " IATA imgs names");
		} catch (IOException e) {
			Log.w(TAG, "Unable to get list of IATA images", e);
		}
	}

	@SuppressLint("DefaultLocale")
	private String getCode(String name) {
		int fileSepInd = name.indexOf('.');
		if (fileSepInd > 0) {
			name = (String) name.subSequence(0, fileSepInd);
		}
		return normalize(name);
	}

	/**
	 * Returns the image and loads it in the cache for subsequent retrieval
	 * 
	 * @param iataCode
	 * @return null if no such IATA code supported
	 */
	public Bitmap getIataImage(String iataCode) {
		if (TextUtils.isEmpty(iataCode) || !mCode2Fname.containsKey(iataCode)) {
			return null;
		}
		Bitmap img = mMemoryCache.get(iataCode);
		if (img == null) {
			try {
				// fname guaranteed to exist
				String fname = mCode2Fname.get(iataCode);
				String fullPath = IATA_PATH + "/" + fname;
				img = TwHelper.SINGLETON.decodeSampledBitmap(mContext, fullPath,
						BITMAP_WID, BITMAP_HEIGHT);
				if (img == null) {
					throw new Exception("Cannot decode image for code " + iataCode);
				}
				mMemoryCache.put(iataCode, img);
			} catch (Exception e) {
				// Just in case there's a outofmemory or something untoward
				Log.w(TAG, "Unable to get bitmap", e);
				return null;
			}
		}
		return img;
	}

	public void clear() {
		mMemoryCache.evictAll();
	}

	public static String normalize(String destination) {
		if (destination == null) {
			return null;
		}
		return destination.toLowerCase();
	}

}

package org.chenhome.tripwatcher.ui;

import java.util.ArrayList;
import java.util.List;

import org.chenhome.tripwatcher.data.Trip;
import org.chenhome.tripwatcher.data.TripTable;
import org.chenhome.tripwatcher.data.TripTable.TripCol;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class TripLoader extends AsyncTaskLoader<List<Trip>> {

	private static final String TAG = "TripLdr";

	private List<Trip> mTrips;

	public TripLoader(Context context) {
		super(context);
	}

	/**
	 * This is where the bulk of our work is done. This function is called in a
	 * background thread and should generate a new set of data to be published
	 * by the loader.
	 */
	@Override
	public List<Trip> loadInBackground() {
		List<Trip> result = new ArrayList<Trip>();
		Cursor c= null;
		try {
			c = getContext().getContentResolver().query(TripTable.CONTENT_URI, TripCol.proj(), null, null, null);
			while (c.moveToNext()) {
				Trip next = Trip.instantiate(c);
				if (next != null && next.isValid()) {
					result.add(next);
				}
			}
		} catch (Exception e) {
			Log.w(TAG,"Unable to query trips from db", e);
		} finally {
			if (c != null) {
				c.close();
			}
		}
		Log.d(TAG, "Found " + result.size() + " trips in the database");
		return result;
	}

	/**
	 * Called when there is new data to deliver to the client. The super class
	 * will take care of delivering it; the implementation here just adds a
	 * little more logic.
	 */
	@Override
	public void deliverResult(List<Trip> trips) {
		// Keep the results for later
		mTrips = trips;
		if (isStarted()) {
			// If the Loader is currently started, we can immediately
			// deliver its results.
			super.deliverResult(trips);
		}
	}

	/**
	 * Handles a request to start the Loader.
	 */
	@Override
	protected void onStartLoading() {
		if (mTrips != null) {
			// If we currently have a result available, deliver it
			// immediately.
			deliverResult(mTrips);
		}

		if (takeContentChanged() || mTrips == null) {
			// If the data has changed since the last time it was loaded
			// or is not currently available, start a load.
			forceLoad();
		}
	}

	/**
	 * Handles a request to stop the Loader.
	 */
	@Override
	protected void onStopLoading() {
		// Attempt to cancel the current load task if possible.
		cancelLoad();
	}

	/**
	 * Handles a request to completely reset the Loader.
	 */
	@Override
	protected void onReset() {
		super.onReset();

		// Ensure the loader is stopped
		onStopLoading();

		// At this point we can release the resources associated with 'apps'
		// if needed.
		if (mTrips != null) {
			mTrips = null;
		}
	}

}

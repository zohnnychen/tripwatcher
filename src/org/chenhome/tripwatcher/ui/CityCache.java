package org.chenhome.tripwatcher.ui;

import org.chenhome.tripwatcher.data.AirportTable;

import android.content.Context;
import android.text.TextUtils;
import android.util.LruCache;

public class CityCache {
	private LruCache<String, String> mCode2City;
	private Context mContext;

	public CityCache(Context context) {
		mCode2City = new LruCache<String, String>(100);
		mContext = context;
	}

	public String getCity(final String code) {
		if (code == null) {
			return null;
		}
		String city = mCode2City.get(code);
		if (city == null) {
			String cityName = AirportTable.queryCityByAirportCode(
					mContext, code);
			if (!TextUtils.isEmpty(cityName)) {
				mCode2City.put(code, cityName);
				return cityName;
			}
			return null;
		}
		return city;
	}
}
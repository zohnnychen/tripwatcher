package org.chenhome.tripwatcher.ui;

import java.util.List;

import org.chenhome.tripwatcher.client.TwApi;
import org.chenhome.tripwatcher.data.Flight;
import org.chenhome.tripwatcher.data.TripMgr;

import android.content.AsyncTaskLoader;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class FlightsLoader extends AsyncTaskLoader<List<Flight>> {

	private static final String TAG = "FltLdr";

	private List<Flight> mFlights;

	// Search terms
	private TripMgr mTripMgr;

	private BroadcastReceiver mOnTripChange;

	public FlightsLoader(Context context, TripMgr tripMgr) {
		super(context);
		if (tripMgr == null) {
			throw new IllegalArgumentException("Need trip specified");
		}
		mTripMgr = tripMgr;
		mOnTripChange = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.d(TAG, "FlightLoader reloading based on trip " + mTripMgr.getTrip());
				onContentChanged();
			}
		};
		LocalBroadcastManager.getInstance(context).registerReceiver(
				mOnTripChange, new IntentFilter(TripMgr.ACTION_TRIP_ONCHANGE));
	}

	/**
	 * This is where the bulk of our work is done. This function is called in a
	 * background thread and should generate a new set of data to be published
	 * by the loader.
	 */
	@Override
	public List<Flight> loadInBackground() {
		List<Flight> result = TwApi.SINGLETON.search(mTripMgr.getTrip());
		Log.d(TAG, "Found " + result.size() + " flights with trip search: "
				+ mTripMgr.getTrip());
		return result;
	}

	/**
	 * Called when there is new data to deliver to the client. The super class
	 * will take care of delivering it; the implementation here just adds a
	 * little more logic.
	 */
	@Override
	public void deliverResult(List<Flight> flights) {
		// Keep the results for later
		mFlights = flights;

		if (isStarted()) {
			// If the Loader is currently started, we can immediately
			// deliver its results.
			super.deliverResult(flights);
		}
	}

	/**
	 * Handles a request to start the Loader.
	 */
	@Override
	protected void onStartLoading() {
		if (mFlights != null) {
			// If we currently have a result available, deliver it
			// immediately.
			deliverResult(mFlights);
		}

		if (takeContentChanged() || mFlights == null) {
			// If the data has changed since the last time it was loaded
			// or is not currently available, start a load.
			forceLoad();
		}
	}

	/**
	 * Handles a request to stop the Loader.
	 */
	@Override
	protected void onStopLoading() {
		// Attempt to cancel the current load task if possible.
		cancelLoad();
	}

	/**
	 * Handles a request to completely reset the Loader.
	 */
	@Override
	protected void onReset() {
		super.onReset();

		// Ensure the loader is stopped
		onStopLoading();

		// At this point we can release the resources associated with 'apps'
		// if needed.
		if (mFlights != null) {
			mFlights = null;
		}
		if (mOnTripChange != null) {
			LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(
					mOnTripChange);
		}
	}

}

package org.chenhome.tripwatcher.ui;

import java.util.List;

import org.chenhome.tripwatcher.R;
import org.chenhome.tripwatcher.TwApp;
import org.chenhome.tripwatcher.data.Flight;
import org.chenhome.tripwatcher.ui.Util.Font;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TopDstAdapter extends ArrayAdapter<Flight> {

	private LayoutInflater mInflater;
	private IataImgCache mCache;
	private CityCache mCityCache;

	public TopDstAdapter(Activity context) {
		super(context, -1);
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mCache = TwApp.getAirportCache(context);
		mCityCache = TwApp.getCityCache(context);
	}

	public void setData(List<Flight> data) {
		clear();
		if (data != null) {
			addAll(data);
		}
	}

	/**
	 * Populate new items in the list.
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (convertView == null) {
			view = mInflater.inflate(R.layout.flt_card, parent, false);
		}

		Flight item = getItem(position);
		String cityName = mCityCache.getCity(item.destination);
		if (cityName == null) {
			// Will later asynchronously update the adapter
			cityName = item.destination;
		}
		SpannableString cityStyle = Util.withCustomTypeface(getContext(),
				cityName, Font.BEBAS);
		((TextView) view.findViewById(R.id.flt_item_city)).setText(cityStyle);
		
		String price = getContext().getString(R.string.flt_price_known,
				(int) item.price);
		Util.setText(view, R.id.flt_item_price, price);

		ImageView imgView = (ImageView) view.findViewById(R.id.flt_item_img);
		String dstCode = IataImgCache.normalize(item.destination);
		Bitmap img = mCache.getIataImage(dstCode);
		if (img != null) {
			imgView.setImageBitmap(img);
		} else {
			imgView.setImageResource(R.drawable.loc_unknown);
		}
		return view;
	}
}

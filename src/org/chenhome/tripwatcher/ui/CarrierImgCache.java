package org.chenhome.tripwatcher.ui;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.util.LruCache;

@SuppressLint("DefaultLocale")
public class CarrierImgCache {
	private static final String TAG = "CarrierCache";
	private LruCache<String, Bitmap> mMemoryCache;

	public CarrierImgCache(Context context) {
		mMemoryCache = new LruCache<String, Bitmap>(100);
	}

	/**
	 * Returns the image and loads it in the cache for subsequent retrieval
	 * 
	 * @param 2-character IATA Carrier code
	 * @return null if no such code supported
	 */
	public Bitmap getImage(final String carrierCode) {
		if (TextUtils.isEmpty(carrierCode)) {
			return null;
		}
		Bitmap img = mMemoryCache.get(carrierCode);
		if (img == null) {
			AsyncTask.execute(new Runnable() {
				@Override
				public void run() {
					getImageFromNetwork(carrierCode);					
				}
				
			});
		}
		return img;
	}

	private Bitmap getImageFromNetwork(String carrierCode) {
		InputStream in = null;
		try {
			URL url = new URL("https://www.tripwatcher.com/img/carrier/"
					+ carrierCode + ".png");
			in = (InputStream) url.getContent();
			if (in != null) {
				Bitmap bitmap = BitmapFactory.decodeStream(in);
				if (bitmap != null) {
					mMemoryCache.put(carrierCode, bitmap);
					return bitmap;
				}
			}
			return null;
		} catch (Exception e) {
			Log.w(TAG, "Unable to get icon for carrier " + carrierCode, e);
			return null;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					Log.w(TAG, "Unable to close stream", e);
				}
			}
		}

	}

	public void clear() {
		mMemoryCache.evictAll();
	}
}

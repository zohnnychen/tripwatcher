package org.chenhome.tripwatcher.ui;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public enum TwHelper {
	// Singleton instance of the factory
	SINGLETON;

	private static final String TAG = "Hlp";

	public Bitmap decodeSampledBitmap(Context context, String assetFullPath, int reqWidth,
			int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();

		InputStream in = null;
		try {
			in = context.getAssets().open(assetFullPath);
			Log.v(TAG, "Trying to decode with filepath " + assetFullPath);
			//
			// // Just get some meta info first
			// options.inJustDecodeBounds = true;
			// BitmapFactory.decodeStream(in, null, options);
			//
			// // Calculate inSampleSize
			// options.inSampleSize = calculateInSampleSize(options, reqWidth,
			// reqHeight);
			//
			// // Decode bitmap with inSampleSize set
			// TODO don't assume downsample rate
			options.inSampleSize = 2;
			options.inJustDecodeBounds = false;
			return BitmapFactory.decodeStream(in, null, options);
		} catch (FileNotFoundException e) {
			Log.w(TAG,
					"Unable to decode to bitmap: " + assetFullPath + ", msg: "
							+ e.getMessage());
			return null;
		} catch (IOException e2) {
			Log.w(TAG,"Unable to open asset at path " + assetFullPath, e2);
			return null;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					Log.w(TAG, "Unable to close bitmap stream", e);
				}
			}
		}
	}


	@SuppressWarnings("unused")
	private int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and
			// width
			final int heightRatio = Math.round((float) height
					/ (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will
			// guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}

}

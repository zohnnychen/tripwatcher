package org.chenhome.tripwatcher.ui;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.chenhome.tripwatcher.R;
import org.chenhome.tripwatcher.TwApp;
import org.chenhome.tripwatcher.data.Flight;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FlightAdapter extends ArrayAdapter<Flight> {

	private static final String TAG = "FltAdap";
	private LayoutInflater mInflater;
	private CityCache mCityCache;
	private CarrierImgCache mCarrierCache;
	private java.text.DateFormat mTimeF;

	public FlightAdapter(Activity context) {
		super(context, -1);
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mCityCache = TwApp.getCityCache(context);
		mCarrierCache = TwApp.getCarrierCache(context);
		mTimeF = DateFormat.getTimeFormat(context);
	}

	public void setData(List<Flight> data) {
		clear();
		if (data != null) {
			addAll(data);
		}
	}

	/**
	 * Populate new items in the list.
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View view = convertView;
		if (view == null) {
			view = mInflater.inflate(R.layout.flt_item, parent, false);
		}

		Flight flt = getItem(position);
		if (flt == null) {
			return view;
		}
		// Cast price to integer
		String price = getContext().getString(R.string.flt_price_known,
				(int)flt.price);
		setText(view, R.id.flt_item_price, price);

		String city = mCityCache.getCity(flt.destination);
		setText(view, R.id.flt_dst,
				getContext().getString(R.string.to_dst, new String[] { city }));
		View outboundV = view.findViewById(R.id.flt_outbound);
		initFlightLeg(outboundV, flt, flt.outbound.carrier,
				flt.outbound.departure, flt.outbound.arrival,
				flt.outbound.durationMinutes);

		View inboundV = view.findViewById(R.id.flt_inbound);
		initFlightLeg(inboundV, flt, flt.inbound.carrier,
				flt.inbound.departure, flt.inbound.arrival,
				flt.inbound.durationMinutes);
		return view;
	}
	
	private static SimpleDateFormat sDurF = new SimpleDateFormat("HH'hr' mm'min'",
			Locale.getDefault());
	private static SimpleDateFormat sDateF = new SimpleDateFormat("EEE, MMM d",
			Locale.getDefault());

	private static String formatDuration(int durationMinutes) {
		long duration = durationMinutes * 60 * 1000;
		String val = sDurF.format(new Date(duration
				- TimeZone.getDefault().getRawOffset()));
		return val;
	}

	private void initFlightLeg(View view, Flight flight, String carrier,
			Date depart, Date arrival, int durationMinutes) {
		ImageView f_carrier = (ImageView) view
				.findViewById(R.id.f_carrier_icon);
		TextView f_carrierCode = (TextView) view
				.findViewById(R.id.f_carrier_code);
		Bitmap bitmap = mCarrierCache.getImage(carrier);
		if (bitmap != null) {
			f_carrier.setImageBitmap(bitmap);
			f_carrierCode.setVisibility(View.GONE);
			f_carrier.setVisibility(View.VISIBLE);
		} else {
			f_carrier.setImageBitmap(null);
			f_carrierCode.setVisibility(View.VISIBLE);
			f_carrierCode.setText(carrier);
			f_carrier.setVisibility(View.GONE);			
		}

		setText(view, R.id.f_depart_date, sDateF.format(depart));
		String val = getContext().getString(R.string.f_leg_time,
				new String[] { mTimeF.format(depart), flight.origin });
		setText(view, R.id.f_depart_time, val);

		String val2 = getContext().getString(R.string.f_leg_time,
				new String[] { mTimeF.format(arrival), flight.destination });
		setText(view, R.id.f_arr_time, val2);

	}

	private void setText(View parent, int id, String str) {
		try {
			((TextView) parent.findViewById(id)).setText(str);
		} catch (Exception e) {
		}
	}
}

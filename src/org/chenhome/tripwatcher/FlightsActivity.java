package org.chenhome.tripwatcher;

import java.util.List;

import org.chenhome.tripwatcher.FlightsActionBar.SearchViewId;
import org.chenhome.tripwatcher.client.TwApi;
import org.chenhome.tripwatcher.data.AirportTable;
import org.chenhome.tripwatcher.data.Flight;
import org.chenhome.tripwatcher.data.Trip;
import org.chenhome.tripwatcher.data.Trip.Month;
import org.chenhome.tripwatcher.data.TripMgr;
import org.chenhome.tripwatcher.ui.CityCache;
import org.chenhome.tripwatcher.ui.DateDialogFragment;
import org.chenhome.tripwatcher.ui.DateDialogFragment.Type;
import org.chenhome.tripwatcher.ui.FlightAdapter;
import org.chenhome.tripwatcher.ui.FlightsLoader;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;

public class FlightsActivity extends Activity implements
		LoaderCallbacks<List<Flight>>, OnItemClickListener {

	private ListView mListView;

	private FlightsActionBar mBarHelper;
	private FlightAdapter mAdapter;
	private TripMgr mTripMgr;
	private OnTripChange mOnTripChangeReceiver;
	private ProgressBar mProgress;
	private View mHeader;

	@SuppressWarnings("unused")
	private CityCache mCityCache;

	private MenuItem mWatchTripAction;

	private static final String TAG = "FltAct";
	private static final int REQ_LOAD_FLIGHTS = 10;
	public static final String EXTRA_TRIP_JSON = "EXTRA_TRIP_JSON";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_flights);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		// Trip state
		Trip trip = initTrip(getIntent());
		mTripMgr = new TripMgr(this, trip);

		// Helpers
		mCityCache = TwApp.getCityCache(this);
		mAdapter = new FlightAdapter(this);
		mOnTripChangeReceiver = new OnTripChange();
		mBarHelper = new FlightsActionBar(this, mTripMgr);

		// View elements
		mListView = (ListView) findViewById(R.id.flt_list);
		mHeader = findViewById(R.id.flt_header);
		initClearAffordance();

		mListView.setEmptyView(findViewById(R.id.flt_empty));
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(this);
		mProgress = (ProgressBar) findViewById(R.id.top_progress);

		// Load results
		getLoaderManager().initLoader(REQ_LOAD_FLIGHTS, null, this);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		String action = intent.getAction();
		final Uri data = intent.getData();
		if (!FlightsActionBar.ACTION_SELECT_AIRPORT.equals(action)
				|| data == null) {
			return;
		}
		final SearchViewId lastSubmitted = mBarHelper.getLastSubmitted();
		if (lastSubmitted != null) {

			AsyncTask.execute(new Runnable() {
				@Override
				public void run() {
					String code = AirportTable.getCodeByAirportUri(
							FlightsActivity.this, data);
					Log.d(TAG,"onNewIntent " + code);
					if (!TextUtils.isEmpty(code)) {
						switch (lastSubmitted) {
						case DEST:
							mTripMgr.setDst(code);
							break;
						case ORIGIN:
							mTripMgr.setOrigin(code);
							break;
						}
					}
				}
			});
		}
		super.onNewIntent(intent);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mBarHelper.onDestroy();
	}

	@Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
	}

	@Override
	protected void onStop() {
		EasyTracker.getInstance().activityStop(this);
		super.onStop();
	}

	@Override
	protected void onPause() {
		if (mOnTripChangeReceiver != null) {
			LocalBroadcastManager.getInstance(this).unregisterReceiver(
					mOnTripChangeReceiver);
		}
		super.onPause();
	}

	@Override
	protected void onResume() {
		IntentFilter f = new IntentFilter(TripMgr.ACTION_TRIP_ONCHANGE);
		LocalBroadcastManager.getInstance(this).registerReceiver(
				mOnTripChangeReceiver, f);

		// Let all observers know the most current state of Trip
		mTripMgr.notifyChange();
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		mBarHelper.onCreateOptions(menu);
		mWatchTripAction = menu.findItem(R.id.menuitem_opt_watch);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return mBarHelper.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.menuitem_byexact:
			showDialog(Type.BY_EXACT);
			return true;
		case R.id.menuitem_bymonth:
			showDialog(Type.BY_MONTH);
			return true;
		case R.id.menuitem_byweekends:
			mTripMgr.setByWeekends();
			return true;
		case R.id.menuitem_opt_share:
			onMenuItemShare(item);
			return true;
		case R.id.menuitem_opt_watch:
			onMenuItemWatch(item);
			return true;

		default:			
			return super.onOptionsItemSelected(item);
		}
	}

	public void onMenuItemWatch(MenuItem item) {
		// If it's curently saved, delete it from the db
		if (mTripMgr.isTripAlreadySaved(this)) {
			mWatchTripAction.setIcon(android.R.drawable.btn_star_big_off);
			mTripMgr.stopWatchingTrip(this);
		} else {			
			Toast.makeText(this, R.string.watching, Toast.LENGTH_SHORT).show();
			mWatchTripAction.setIcon(android.R.drawable.btn_star_big_on);
			mTripMgr.insertTrip(this);
		}
	}

	public void onMenuItemShare(MenuItem item) {
		Trip.startShareChooser(mTripMgr.getTrip(), this);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		try {
			Flight flt = (Flight) arg0.getItemAtPosition(position);
			if (flt != null && !TextUtils.isEmpty(flt.link)) {
				Uri uri = Uri.parse(flt.link);
				startActivity(new Intent(Intent.ACTION_VIEW, uri));
			}
		} catch (Exception e) {
			Log.w(TAG, "Unable to handle item click", e);
		}
	}

	@Override
	public Loader<List<Flight>> onCreateLoader(int id, Bundle args) {
		return new FlightsLoader(this, mTripMgr);
	}

	@Override
	public void onLoadFinished(Loader<List<Flight>> loader, List<Flight> data) {
		// Set the new data in the adapter.
		mAdapter.setData(data);
		mTripMgr.getTrip().price = getLowestPrice(data);
		showProgress(false);
	}

	private int getLowestPrice(List<Flight> data) {
		int lowest = Integer.MAX_VALUE;
		for (Flight f:data){
			if (f.price > 0 && f.price < lowest){
				lowest = (int)f.price;
			}
		}
		if (lowest == Integer.MAX_VALUE) {
			return 0;
		}
		return lowest;
	}

	@Override
	public void onLoaderReset(Loader<List<Flight>> loader) {
		// Clear the data in the adapter.
		mAdapter.setData(null);
	}


	private Trip initTrip(Intent intent) {
		Trip trip = new Trip();
		if (intent != null && intent.hasExtra(EXTRA_TRIP_JSON)) {
			String tripJson = intent.getStringExtra(EXTRA_TRIP_JSON);
			if (!TextUtils.isEmpty(tripJson)) {
				Log.d(TAG, "Initialized trip " + tripJson);
				trip = Trip.instantiate(tripJson);
			}
		}
		return trip;
	}

	private void showDialog(DateDialogFragment.Type type) {
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		Fragment prev = getFragmentManager().findFragmentByTag(type.name());
		if (prev != null) {
			ft.remove(prev);
		}
		ft.addToBackStack(null);

		// Create and show the dialog.
		DialogFragment newFragment = DateDialogFragment.newInstance(type);
		newFragment.show(ft, type.name());
	}

	private void showProgress(boolean show) {
		mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
		try {
			mListView.getEmptyView().findViewById(R.id.top_empty_select)
					.setVisibility(show ? View.GONE : View.VISIBLE);
			TextView t = (TextView) mListView.getEmptyView().findViewById(
					R.id.flt_empty_title);
			t.setText(show ? R.string.wait_search : R.string.no_flights);
		} catch (Exception e) {
			Log.w(TAG, "Unable to toggle progress", e);
		}
	}

	private void initHeaderTitle(TextView title, TripMgr tripMgr) {
		Month selectedMonth = tripMgr.getSelectedMonth();
		if (selectedMonth != null) {
			title.setText(getString(R.string.criteria_bymonth,
					selectedMonth.getDisplayMonth(this)));
		} else if (tripMgr.isWeekendSet()) {
			title.setText(R.string.criteria_byweekend);
		} else if (tripMgr.isDatesSet()) {
			title.setText(getString(R.string.criteria_byexact,
					tripMgr.getDisplayDeparting(),
					tripMgr.getDisplayReturning()));
		}

	}

	private void resetSearch() {
		Log.d(TAG, "Reseting search");
		if (mTripMgr != null) {
			mTripMgr.clear();
			mTripMgr.notifyChange();
		}

	}

	public TripMgr getTripMgr() {
		return mTripMgr;
	}

	private void initClearAffordance() {
		View header = findViewById(R.id.flt_header);
		View clear = header.findViewById(R.id.flt_header_clear);
		clear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				resetSearch();
			}
		});

		findViewById(R.id.top_empty_select).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						resetSearch();
					}
				});
		
		findViewById(R.id.top_empty_choose_origin).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (mBarHelper != null) {
							resetSearch();
							mBarHelper.expandFromAction();
						}
					}
				});

	}

	private class OnTripChange extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null
					&& intent.getAction().equals(TripMgr.ACTION_TRIP_ONCHANGE)) {

				new AsyncTask<Void, Void, Boolean>() {
					@Override
					protected Boolean doInBackground(Void... params) {
						return mTripMgr.isTripAlreadySaved(FlightsActivity.this);
					}

					@Override
					protected void onPostExecute(Boolean isTripAlreadySaved) {
						boolean isEnableAction = !isTripAlreadySaved;
						if (mWatchTripAction != null) {
							mWatchTripAction
									.setIcon(isEnableAction ? android.R.drawable.btn_star_big_off
											: android.R.drawable.btn_star_big_on);
						}
					}

				}.execute(null, null, null);
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Log.d(TAG, "On Trip changed");

						// If a change to the trip kicked off the loader,
						// show the progress bar
						Loader<Flight> ldr = getLoaderManager().getLoader(
								REQ_LOAD_FLIGHTS);
						if (ldr != null && ldr.isStarted()) {
							showProgress(true);
						}

						if (mTripMgr.hasCriteriaSet()) {
							mHeader.setVisibility(View.VISIBLE);
							initHeaderTitle(
									(TextView) mHeader
											.findViewById(R.id.flt_header_title),
									mTripMgr);
						} else {
							mHeader.setVisibility(View.GONE);
						}
						mAdapter.notifyDataSetChanged();
					}
				});
			}

		}

	}

}

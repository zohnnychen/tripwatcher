package org.chenhome.tripwatcher;

import org.chenhome.tripwatcher.data.AirportLoader;
import org.chenhome.tripwatcher.data.TwAlarmListener;
import org.chenhome.tripwatcher.data.TwService;
import org.chenhome.tripwatcher.data.TwService.Cmd;
import org.chenhome.tripwatcher.ui.Util;
import org.chenhome.tripwatcher.ui.Util.Font;

import com.commonsware.cwac.wakeful.WakefulIntentService;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

public class TwSplashActivity extends Activity {
	private static final String TAG = "TwSplash";
	private static final long MAX_MILLIS = 3500;
	public static final String DEFAULT_NEAREST_AIRPORTCODE = "SFO";
	private TwServiceRcv mTwServiceRcv;
	private Handler mHandler;
	private TextView mComment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_splash);
		mComment = (TextView) findViewById(R.id.splash_comment);
		Util.setTypeface(this, (TextView) findViewById(R.id.splash_appname),
				Font.BEBAS);

		AsyncTask.execute(new Runnable() {
			@Override
			public void run() {
				if (AirportLoader.SINGLETON.numAirports(TwSplashActivity.this) > 0
						&& TwService
								.isNearestLocationSet(TwSplashActivity.this)) {
					// bail from splash immediately
					finishSplash();
				}
			}
		});

		// Else wait for a while and try to load airports and the location
		// Listen for load airports and get location request to be done
		mTwServiceRcv = new TwServiceRcv();
		mHandler = new Handler();

		// Schedule but don't force checking
		Log.i(TAG,"Scheduling periodic price checks");
		WakefulIntentService.scheduleAlarms(new TwAlarmListener(), this, false);
	}

	private void finishSplash() {
		startActivity(new Intent(TwSplashActivity.this, TopDstActivity.class));
		finish();
	}

	@Override
	protected void onStart() {
		// Load airports, takes around 10 seconds
		mTwServiceRcv.register();
		WakefulIntentService.sendWakefulWork(this,
				Cmd.SVC_LOAD_AIRPORTS.toStartCmd(this));
		mComment.setText(R.string.getting_airports);
		super.onStart();
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (mTwServiceRcv != null) {
			unregisterReceiver(mTwServiceRcv);
		}
		// Remove all callbacks
		mHandler.removeCallbacksAndMessages(null);
	}

	public class TwServiceRcv extends BroadcastReceiver {

		private final String ACTION_LOAD_AIRPORT_FINISHED = Cmd.SVC_LOAD_AIRPORTS
				.toOnCmdFinish().getAction();
		private final String ACTION_LOCATION_RESPONSE = Cmd.SVC_RESPONSE_LOCATION
				.toOnCmdFinish().getAction();

		@Override
		public void onReceive(Context context, Intent i) {
			String action = i.getAction();
			Log.d(TAG, "Received action " + action);
			if (action.equals(ACTION_LOAD_AIRPORT_FINISHED)) {
				Log.d(TAG, "Airports loaded. Requesting location now");
				WakefulIntentService.sendWakefulWork(TwSplashActivity.this,
						Cmd.SVC_REQUEST_LOCATION
								.toStartCmd(TwSplashActivity.this));
				mComment.setText(R.string.getting_location);

				mHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						Log.d(TAG, "Max delay reached. Exiting splash");
						if (TextUtils.isEmpty(TwApp
								.getNearestAirport(TwSplashActivity.this))) {
							// set to a default location
							TwApp.setNearestAirport(TwSplashActivity.this,
									DEFAULT_NEAREST_AIRPORTCODE);
						}
						finishSplash();
					}

				}, MAX_MILLIS);

			} else if (action.equals(ACTION_LOCATION_RESPONSE)
					&& i.hasExtra(Cmd.EXTRA_NEAREST_AIRPORTCODE)) {
				String nearest = i
						.getStringExtra(Cmd.EXTRA_NEAREST_AIRPORTCODE);
				Log.d(TAG, "All done! Got closest airport " + nearest);
				finishSplash();
			}
		}

		public void register() {
			IntentFilter filter = new IntentFilter(ACTION_LOAD_AIRPORT_FINISHED);
			IntentFilter filter2 = new IntentFilter(ACTION_LOCATION_RESPONSE);
			registerReceiver(this, filter);
			registerReceiver(this, filter2);
		}
	}

}

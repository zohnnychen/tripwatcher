package org.chenhome.tripwatcher;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.chenhome.tripwatcher.data.AirportTable.AirportCol;
import org.chenhome.tripwatcher.data.Trip;
import org.chenhome.tripwatcher.data.TwService;
import org.chenhome.tripwatcher.ui.MyTripsAdapter;
import org.chenhome.tripwatcher.ui.TripLoader;
import org.chenhome.tripwatcher.ui.Util;
import org.chenhome.tripwatcher.ui.Util.Font;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Intent;
import android.content.Loader;
import android.os.Bundle;
import android.text.SpannableString;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;

@SuppressLint("SimpleDateFormat")
public class MyTripsActivity extends Activity implements OnItemClickListener,
		LoaderCallbacks<List<Trip>> {

	private ListView mListView;
	private ProgressBar mProgress;
	private MyTripsAdapter mAdapter;
	private TextView mHeader;
	private static final String TAG = "MyTripsAct";
	private static final int REQ_LOAD_TRIPS = 101;
	private static final SimpleDateFormat sF = new SimpleDateFormat("K:mma MMM dd");
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_trips);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setDisplayShowTitleEnabled(true);
		SpannableString styled = Util.withCustomTypeface(this,
				getString(R.string.title_mytrips), Font.BEBAS);
		getActionBar().setTitle(styled);

		mHeader = (TextView)findViewById(R.id.topdst_header_orgin);
		// Helpers
		mAdapter = new MyTripsAdapter(this);

		// View elements
		mListView = (ListView) findViewById(R.id.trip_list);
		mListView.setEmptyView(findViewById(R.id.trip_empty));
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(this);
		mProgress = (ProgressBar) findViewById(R.id.trip_progress);

		// Load results
		getLoaderManager().initLoader(REQ_LOAD_TRIPS, null, this);
	}

	@Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
	}

	@Override
	protected void onStop() {
		Log.d(TAG, "onStop");
		EasyTracker.getInstance().activityStop(this);
		super.onStop();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		try {
			Trip trip = (Trip) arg0.getItemAtPosition(position);
			if (trip != null && trip.isValid()
					&& AirportCol.isValidCode(trip.org)) {
				startFlightActivity(trip);
			}
		} catch (Exception e) {
			Log.w(TAG, "Unable to handle item click", e);
		}
	}

	@Override
	public Loader<List<Trip>> onCreateLoader(int id, Bundle args) {
		return new TripLoader(this);
	}

	@Override
	public void onLoadFinished(Loader<List<Trip>> loader, List<Trip> data) {
		// Set the new data in the adapter.
		if (data != null && data.size() > 0) {
			Date lastCheck = TwService.getLastPriceCheck(this);
			if (lastCheck != null) {
				// TODO not very localized
				mHeader.setText(getString(R.string.lastcheck_specific, sF.format(lastCheck)));
				mHeader.setVisibility(View.VISIBLE);
			}
		} else {
			mHeader.setVisibility(View.GONE);
		}
		mAdapter.setData(data);
		mProgress.setVisibility(View.GONE);
	}

	@Override
	public void onLoaderReset(Loader<List<Trip>> loader) {
		// Clear the data in the adapter.
		mAdapter.setData(null);
	}

	private void startFlightActivity(Trip clone) {
		Intent i = new Intent(this, FlightsActivity.class);
		i.putExtra(FlightsActivity.EXTRA_TRIP_JSON, clone.toJson());
		startActivity(i);
	}

}

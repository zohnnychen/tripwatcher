package org.chenhome.tripwatcher;

import org.chenhome.tripwatcher.data.AirportTable;
import org.chenhome.tripwatcher.data.Trip;
import org.chenhome.tripwatcher.data.TripMgr;
import org.chenhome.tripwatcher.ui.CityCache;
import org.chenhome.tripwatcher.ui.Util;
import org.chenhome.tripwatcher.ui.Util.Font;

import android.app.Activity;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnActionExpandListener;
import android.widget.SearchView;
import android.widget.SearchView.OnCloseListener;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.SearchView.OnSuggestionListener;

/**
 * Helper for managing state and visual-state of action bar
 */
public class FlightsActionBar extends BroadcastReceiver {

	public static final String ACTION_SELECT_AIRPORT = "org.chenhome.tripwatcher.SELECT_AIRPORT";

	protected static final String TAG = "BarHlp";
	private Activity mAct;
	private TripMgr mTripMgr;

	private MenuItem mToItem;
	private MenuItem mFromItem;
	private SearchViewId mLastSubmitted = null;

	private CityCache mCityCache;

	public enum SearchViewId {
		DEST, ORIGIN;
	}

	public SearchViewId getLastSubmitted() {
		return mLastSubmitted;
	}

	public FlightsActionBar(Activity activity, TripMgr mgr) {
		mAct = activity;
		mAct.getActionBar().setDisplayShowTitleEnabled(true);
		SpannableString styled = Util.withCustomTypeface(activity,
				activity.getString(R.string.title_generic), Font.BEBAS);
		mAct.getActionBar().setTitle(styled);
		mTripMgr = mgr;
		mCityCache = TwApp.getCityCache(activity);
		LocalBroadcastManager.getInstance(activity).registerReceiver(this,
				new IntentFilter(TripMgr.ACTION_TRIP_ONCHANGE));
	}

	public void onDestroy() {
		LocalBroadcastManager.getInstance(mAct).unregisterReceiver(this);
	}

	public void onCreateOptions(final Menu menu) {
		MenuInflater inflater = mAct.getMenuInflater();
		inflater.inflate(R.menu.menu_flights, menu);
		mToItem = menu.findItem(R.id.menuitem_opt_to);
		mFromItem = menu.findItem(R.id.menuitem_opt_from);
		initMenuItem(mToItem, new SearchViewListener(SearchViewId.DEST));
		initMenuItem(mFromItem, new SearchViewListener(SearchViewId.ORIGIN));
	}

	public void expandFromAction() {
		if (mFromItem != null) {
			mFromItem.expandActionView();
		}
	}
	
	private void initMenuItem(MenuItem item, SearchViewListener listener) {
		SearchManager searchManager = (SearchManager) mAct
				.getSystemService(Context.SEARCH_SERVICE);
		item.setOnActionExpandListener(listener);
		SearchView search = (SearchView) item.getActionView();
		search.setSearchableInfo(searchManager.getSearchableInfo(mAct
				.getComponentName()));
		search.setOnQueryTextListener(listener);
		search.setOnCloseListener(listener);
		search.setOnSuggestionListener(listener);
	}

	private class SearchViewListener implements OnQueryTextListener,
			OnActionExpandListener, OnCloseListener, OnSuggestionListener {
		private SearchViewId mViewId;

		public SearchViewListener(SearchViewId searchViewId) {
			mViewId = searchViewId;
		}

		public boolean onQueryTextChange(String arg0) {
			return false;
		}

		public boolean onQueryTextSubmit(final String code) {
			Log.d(TAG, "Query:" + code);
			mToItem.collapseActionView();
			mFromItem.collapseActionView();
			mLastSubmitted = mViewId;

			AsyncTask.execute(new Runnable() {
				@Override
				public void run() {
					Uri airport = AirportTable.queryByAirportCode(mAct, code);
					if (airport != null) {
						// We found a single match
						switch (mViewId) {
						case DEST:
							mTripMgr.setDst(code);
							break;
						case ORIGIN:
							mTripMgr.setOrigin(code);
							break;
						}
					}
				}
			});

			return false;
		}

		@Override
		public boolean onMenuItemActionCollapse(MenuItem item) {
			Log.d(TAG, "Collapsed" + this);
			mFromItem.setVisible(true);
			mToItem.setVisible(true);
			return true;
		}

		@Override
		public boolean onMenuItemActionExpand(MenuItem item) {
			mFromItem.setVisible(false);
			mToItem.setVisible(false);
			return true;
		}

		@Override
		public boolean onClose() {
			mFromItem.collapseActionView();
			mToItem.collapseActionView();
			return true;
		}

		@Override
		public boolean onSuggestionClick(int position) {
			mFromItem.collapseActionView();
			mToItem.collapseActionView();
			mLastSubmitted = mViewId;
			return false;
		}

		@Override
		public boolean onSuggestionSelect(int position) {
			return false;
		}

	}

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "OnReceive");
		if (intent != null
				&& intent.getAction().equals(TripMgr.ACTION_TRIP_ONCHANGE)) {
			// Change the actionbar
			initAirportMenuItems(mFromItem, mToItem);
		}
	}

	private void initAirportMenuItems(MenuItem fromItem, MenuItem toItem) {
		Trip trip = mTripMgr.getTrip();
		if (trip != null) {
			initAirportMenuItem(trip.org, true, fromItem);
			initAirportMenuItem(trip.dst, false, toItem);
		}
	}

	private void initAirportMenuItem(String code, boolean isFrom, MenuItem item) {
		if (item == null) {
			return;
		}
		int emptyTitleRsc = isFrom ? R.string.opt_from : R.string.opt_to;
		int titleRsc = isFrom ? R.string.opt_from2 : R.string.opt_to2;
		SearchView search = (SearchView) item.getActionView();
		if (TextUtils.isEmpty(code)) {
			search.setQueryHint(mAct.getString(R.string.search_hint));
			item.setTitle(emptyTitleRsc);
		} else {
			String city = Util.getCity(code, mCityCache);
			search.setQueryHint(mAct.getString(titleRsc, new String[] { city }));
			item.setTitle(mAct.getString(titleRsc, new String[] { code }));
		}
	}

	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem from = menu.findItem(R.id.menuitem_opt_from);
		MenuItem to = menu.findItem(R.id.menuitem_opt_to);
		if (from != null && to != null) {
			initAirportMenuItems(from, to);
		}
		MenuItem watch = menu.findItem(R.id.menuitem_opt_watch);
		MenuItem share = menu.findItem(R.id.menuitem_opt_share);
		watch.setVisible(!TextUtils.isEmpty(mTripMgr.getTrip().dst));
		share.setVisible(!TextUtils.isEmpty(mTripMgr.getTrip().dst));		

		return true;
	}

}

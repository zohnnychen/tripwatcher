package org.chenhome.tripwatcher;

import org.chenhome.tripwatcher.data.TwService.Cmd;
import org.chenhome.tripwatcher.ui.CarrierImgCache;
import org.chenhome.tripwatcher.ui.CityCache;
import org.chenhome.tripwatcher.ui.IataImgCache;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import android.util.Log;

public class TwApp extends Application {

	private static final String TAG = "TwpApp";

	private final String ACTION_LOCATION_RESPONSE = Cmd.SVC_RESPONSE_LOCATION
			.toOnCmdFinish().getAction();

	private IataImgCache mIataImgCache;
	private CityCache mCityCache;
	private CarrierImgCache mCarrierCache;
	private String mNearestAirportCode;

	private NearestLocationRcv mLocRcv;

	@Override
	public void onCreate() {
		super.onCreate();
		mIataImgCache = new IataImgCache(getApplicationContext());
		mCityCache = new CityCache(getApplicationContext());
		mCarrierCache = new CarrierImgCache(getApplicationContext());

		mLocRcv = new NearestLocationRcv();
		registerReceiver(mLocRcv, new IntentFilter(ACTION_LOCATION_RESPONSE));
	}

	@Override
	public void onLowMemory() {
		mIataImgCache.clear();
		mCarrierCache.clear();
		super.onLowMemory();
	}

	@Override
	public void onTerminate() {
		mIataImgCache.clear();
		mCarrierCache.clear();
		if (mLocRcv != null) {
			unregisterReceiver(mLocRcv);
		}
		super.onTerminate();
	}

	public static IataImgCache getAirportCache(Activity act) {
		TwApp app = (TwApp) act.getApplication();
		return app.mIataImgCache;
	}

	public static CarrierImgCache getCarrierCache(Activity act) {
		TwApp app = (TwApp) act.getApplication();
		return app.mCarrierCache;
	}

	public static CityCache getCityCache(Activity act) {
		TwApp app = (TwApp) act.getApplication();
		return app.mCityCache;
	}

	public static String getNearestAirport(Activity act) {
		TwApp app = (TwApp) act.getApplication();
		return app.mNearestAirportCode;
	}

	public static void setNearestAirport(Activity act, String nearestCode) {
		TwApp app = (TwApp) act.getApplication();
		Log.d(TAG, "Overwriting nearest airport from "
				+ app.mNearestAirportCode + " to " + nearestCode);
		app.mNearestAirportCode = nearestCode;
	}

	public class NearestLocationRcv extends BroadcastReceiver {

		private static final String TAG = "NearestLocRcv";

		@Override
		public void onReceive(Context context, Intent i) {
			String action = i.getAction();
			if (action.equals(ACTION_LOCATION_RESPONSE)
					&& i.hasExtra(Cmd.EXTRA_NEAREST_AIRPORTCODE)) {
				String nearest = i
						.getStringExtra(Cmd.EXTRA_NEAREST_AIRPORTCODE);
				if (!TextUtils.isEmpty(nearest)) {
					Log.d(TAG, "Setting closest airport " + nearest);
					mNearestAirportCode = nearest;
				}
			}
		}

	}
}

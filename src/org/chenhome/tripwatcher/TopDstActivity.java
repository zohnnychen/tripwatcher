package org.chenhome.tripwatcher;

import java.text.SimpleDateFormat;
import java.util.List;

import org.chenhome.tripwatcher.data.AirportTable;
import org.chenhome.tripwatcher.data.AirportTable.AirportCol;
import org.chenhome.tripwatcher.data.Flight;
import org.chenhome.tripwatcher.data.Trip;
import org.chenhome.tripwatcher.data.TripMgr;
import org.chenhome.tripwatcher.data.TwService.Cmd;
import org.chenhome.tripwatcher.ui.CityCache;
import org.chenhome.tripwatcher.ui.FlightsLoader;
import org.chenhome.tripwatcher.ui.TopDstAdapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;

@SuppressLint("SimpleDateFormat")
public class TopDstActivity extends Activity implements
		LoaderCallbacks<List<Flight>>, OnItemClickListener {

	private TripMgr mTripMgr;

	private ListView mListView;
	private TopDstActionBar mBarHelper;
	private TopDstAdapter mAdapter;
	private OnTripChange mOnTripChangeReceiver;
	private ProgressBar mProgress;

	private BroadcastReceiver mOnLocation;

	private TextView mHeader;
	private CityCache mCityCache;

	private ActionBarDrawerToggle mDrawerToggle;

	private DrawerLayout mDrawer;

	private static final String TAG = "TopAct";
	private static final int REQ_LOAD_FLIGHTS = 10;
	private static final String PREF_JSON_TRIP = "PREF_JSON_TRIP";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_topdst);

		// Trip state
		Trip trip = initTrip();
		mTripMgr = new TripMgr(this, trip);
		mCityCache = TwApp.getCityCache(this);

		// Helpers
		mAdapter = new TopDstAdapter(this);
		mOnTripChangeReceiver = new OnTripChange();
		mBarHelper = new TopDstActionBar(this, mTripMgr);

		// View elements
		mListView = (ListView) findViewById(R.id.top_list);
		mListView.setEmptyView(findViewById(R.id.top_empty));
		findViewById(R.id.top_empty_select).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						onTopDstHeaderClick(null);
					}
				});
			}
		});
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(this);
		mProgress = (ProgressBar) findViewById(R.id.top_progress);
		mHeader = (TextView) findViewById(R.id.topdst_header_orgin);

		// Init drawer
		initDrawer();

		// Load results
		getLoaderManager().initLoader(REQ_LOAD_FLIGHTS, null, this);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	private void initDrawer() {		
		mDrawer = (DrawerLayout) findViewById(R.id.top_drawer);
		mDrawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

		ListView drawerList = (ListView) findViewById(R.id.top_left_drawer);

		// Set the adapter for the list view
		drawerList.setAdapter(new DrawerItemAdapter(this));
		// Set the list's click listener
		drawerList.setOnItemClickListener(new DrawerItemClickListener());

		// enable ActionBar app icon to behave as action to toggle nav drawer
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer,
				R.drawable.ic_drawer, 0, 0) {
			public void onDrawerClosed(View view) {
			}

			public void onDrawerOpened(View drawerView) {
			}
		};
		mDrawer.setDrawerListener(mDrawerToggle);

	}

	@Override
	protected void onNewIntent(Intent intent) {
		String action = intent.getAction();
		final Uri data = intent.getData();
		// Specified by searchable.xml. Sent whenever search query submitted to
		// this activity
		if (!FlightsActionBar.ACTION_SELECT_AIRPORT.equals(action)
				|| data == null) {
			return;
		}

		AsyncTask.execute(new Runnable() {
			@Override
			public void run() {
				String code = AirportTable.getCodeByAirportUri(
						TopDstActivity.this, data);
				Log.d(TAG,"onNewIntent " + code);
				if (!TextUtils.isEmpty(code)) {
					mTripMgr.setOrigin(code);
				}
			}
		});
		super.onNewIntent(intent);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mBarHelper.onDestroy();
		// Save trip into preferences
		Trip trip = mTripMgr.getTrip();
		if (trip != null && trip.isValid()) {
			String json = trip.toJson();
			if (!TextUtils.isEmpty(json)) {
				Log.d(TAG, "Saving trip " + json);
				Editor e = getSharedPreferences(TAG, Context.MODE_PRIVATE)
						.edit();
				e.putString(PREF_JSON_TRIP, json);
				e.commit();
			}
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
	}

	@Override
	protected void onStop() {
		Log.d(TAG, "onStop");
		EasyTracker.getInstance().activityStop(this);
		super.onStop();
	}

	@Override
	protected void onPause() {
		Log.d(TAG, "onP");
		if (mOnTripChangeReceiver != null) {
			LocalBroadcastManager.getInstance(this).unregisterReceiver(
					mOnTripChangeReceiver);
		}
		if (mOnLocation != null) {
			try {
				unregisterReceiver(mOnLocation);
			} catch (Exception e) {
				Log.w(TAG, "Unable to unregister location receiver");
			}
		}
		super.onPause();
	}

	@Override
	protected void onResume() {
		Log.d(TAG, "onResume");
		IntentFilter f = new IntentFilter(TripMgr.ACTION_TRIP_ONCHANGE);
		LocalBroadcastManager.getInstance(this).registerReceiver(
				mOnTripChangeReceiver, f);

		// Let all observers know the most current state of Trip
		mTripMgr.notifyChange();

		String nearest = TwApp.getNearestAirport(this);
		if (TextUtils.isEmpty(nearest) || !AirportCol.isValidCode(nearest)) {
			Log.d(TAG,
					"Don't know nearest airport. Will wait for notification of nearest. If we can't figure out nearest, will set origin airport to some default after a short delay");
			mOnLocation = new BroadcastReceiver() {
				@Override
				public void onReceive(Context context, Intent i) {
					if (i.hasExtra(Cmd.EXTRA_NEAREST_AIRPORTCODE)) {
						String nearest = i
								.getStringExtra(Cmd.EXTRA_NEAREST_AIRPORTCODE);
						if (!TextUtils.isEmpty(nearest)
								&& AirportCol.isValidCode(nearest)
								&& TextUtils.isEmpty(mTripMgr.getTrip().org)) {
							Log.d(TAG, "Setting closest airport to " + nearest);
							mTripMgr.setOrigin(nearest);
						}
					}
				}
			};
			registerReceiver(mOnLocation, new IntentFilter(
					Cmd.SVC_RESPONSE_LOCATION.toOnCmdFinish().getAction()));

		} else {
			if (TextUtils.isEmpty(mTripMgr.getTrip().org)) {
				Log.d(TAG, "Already know nearest airport is " + nearest);
				mTripMgr.setOrigin(nearest);
			}
		}
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		mBarHelper.onCreateOptions(menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return mBarHelper.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		// The action bar home/up action should open or close the drawer.
		// ActionBarDrawerToggle will take care of this.
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		switch (item.getItemId()) {
		case R.id.menuitem_opt_search:
			Trip clone = new Trip();
			clone.org = mTripMgr.getTrip().org;
			startFlightActivity(clone);
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		try {
			Flight flt = (Flight) arg0.getItemAtPosition(position);
			if (AirportCol.isValidCode(flt.destination)) {
				Trip clone = new Trip();
				clone.org = mTripMgr.getTrip().org;
				clone.dst = flt.destination;
				startFlightActivity(clone);
			}
		} catch (Exception e) {
			Log.w(TAG, "Unable to handle item click", e);
		}
	}

	@Override
	public Loader<List<Flight>> onCreateLoader(int id, Bundle args) {
		return new FlightsLoader(this, mTripMgr);
	}

	@Override
	public void onLoadFinished(Loader<List<Flight>> loader, List<Flight> data) {
		// Set the new data in the adapter.
		mAdapter.setData(data);
		boolean isEmpty = (data == null || data.size() == 0);
		// hide the header
		mHeader.setVisibility(isEmpty ? View.GONE : View.VISIBLE);
		mProgress.setVisibility(View.GONE);
	}

	@Override
	public void onLoaderReset(Loader<List<Flight>> loader) {
		// Clear the data in the adapter.
		mAdapter.setData(null);
	}

	public void onTopDstHeaderClick(View view) {
		Log.d(TAG, "onTopDstHeader click");
		if (mBarHelper != null) {
			mBarHelper.expandFromAction();
		}
	}

	public void onSearchAction(MenuItem item) {
		Trip clone = new Trip();
		clone.org = mTripMgr.getTrip().org;
		startFlightActivity(clone);
	}

	private class DrawerItemClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			Log.d(TAG, "onItem click pos " + position);
			NavItem item = (NavItem) arg0.getItemAtPosition(position);
			if (item == null) {
				return;
			}
			switch (item) {
			case MY_TRIPS:
				// my trips
				startActivity(new Intent(TopDstActivity.this,
						MyTripsActivity.class));
				break;
			case FLIGHTS:
				// flights
				onSearchAction(null);
				break;
			default:
				// do nothing
			}
			if (mDrawer != null) {
				mDrawer.closeDrawers();
			}
		}
	}

	private class OnTripChange extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null
					&& intent.getAction().equals(TripMgr.ACTION_TRIP_ONCHANGE)) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Log.d(TAG, "On Trip changed");

						// If a change to the trip kicked off the loader,
						// show the progress bar
						Loader<Flight> ldr = getLoaderManager().getLoader(
								REQ_LOAD_FLIGHTS);
						if (ldr != null && ldr.isStarted()) {
							mProgress.setVisibility(View.VISIBLE);
						}
						mAdapter.notifyDataSetChanged();
						String origin = mTripMgr.getTrip().org;
						if (!TextUtils.isEmpty(origin)
								&& AirportCol.isValidCode(origin)) {
							// Refresh the header
							String city = mCityCache.getCity(origin);
							if (TextUtils.isEmpty(city)) {
								city = origin;
							}
							setFromToHeaderText(city);
						}
					}
				});
			}

		}

	}

	private void setFromToHeaderText(String city) {
		// string is prefix + city + postfix
		String prefix = getString(R.string.topdst_header_specific1);
		String postfix = getString(R.string.topdst_header_specific2);
		String all = prefix + " " + city + " " + postfix;
		final SpannableStringBuilder sb = new SpannableStringBuilder(all);
		final ForegroundColorSpan fcs = new ForegroundColorSpan(getResources()
				.getColor(android.R.color.holo_blue_dark));
		final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);

		int start = prefix.length();
		int end = start + city.length() + 1;
		sb.setSpan(fcs, start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
		sb.setSpan(bss, start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
		mHeader.setText(sb);
	}

	private void startFlightActivity(Trip clone) {
		Intent i = new Intent(this, FlightsActivity.class);
		i.putExtra(FlightsActivity.EXTRA_TRIP_JSON, clone.toJson());
		startActivity(i);
	}

	private Trip initTrip() {
		// First try to marshall from preferences, if it was saved before
		SharedPreferences pref = getSharedPreferences(TAG, Context.MODE_PRIVATE);
		String tripJson = pref.getString(PREF_JSON_TRIP, null);
		if (tripJson != null) {
			Trip trip = Trip.instantiate(tripJson);
			if (trip.isValid()) {
				Log.d(TAG, "Loaded trip from pref " + tripJson);
				return trip;
			}
		}
		// Else start with an empty trip
		return new Trip();
	}

	private enum NavItem {
		MY_TRIPS(android.R.drawable.btn_star_big_on, R.string.title_mytrips), FLIGHTS(
				R.drawable.ic_flights, R.string.title_generic);
		private NavItem(int imageRes, int titleRes) {
			imageResId = imageRes;
			titleResId = titleRes;
		}

		public int titleResId;
		public int imageResId;
	}

	public class DrawerItemAdapter extends ArrayAdapter<NavItem> {

		public DrawerItemAdapter(Context context) {
			super(context, -1, NavItem.values());
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = getLayoutInflater().inflate(R.layout.topdst_drawer_item,
					null);
			NavItem item = getItem(position);

			ImageView i = (ImageView) v.findViewById(R.id.draweritem_image);
			i.setImageResource(item.imageResId);

			TextView t = (TextView) v.findViewById(R.id.draweritem_text);
			t.setText(item.titleResId);
			return v;
		}

	}

}

package org.chenhome.tripwatcher.client;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpStatus;
import org.chenhome.tripwatcher.data.Flight;
import org.chenhome.tripwatcher.data.Trip;
import org.chenhome.tripwatcher.data.Trip.Month;
import org.chenhome.tripwatcher.data.Trip.Stopover;
import org.chenhome.tripwatcher.data.TwProvider;

import android.net.Uri;
import android.net.Uri.Builder;
import android.text.TextUtils;
import android.util.Log;

/**
 * Tripwatcher API client
 */
public enum TwApi {
	SINGLETON;

	// NOTE that this is HTTPS
	private static Uri BASE_URI = Uri
			.parse("http://www.tripwatcher.com/api/1.0/flights");
	private static final String TAG = "TwApi";

	public List<Flight> search(Trip trip) {
		Uri queryUrl = buildQueryUrl(trip);
		String json = getJsonResp(queryUrl);
		if (json == null) {
			return new ArrayList<Flight>();
		}
		StringReader reader = null;
		List<Flight> result = new ArrayList<Flight>();
		try {
			reader = new StringReader(json);
			TwApiResp resp = TwApiResp.fromJson(reader);
			Log.i(TAG, "Got " + resp.resultCount + " flights using URL "
					+ queryUrl);
			for (Flight f : resp.flights) {
				result.add(f);
			}
			return result;
		} catch (Exception e) {
			Log.w(TAG, "Unable to parse json\n\n" + json, e);
			return new ArrayList<Flight>();
		} finally {
			if (reader != null) {
				reader.close();
			}
		}

	}

	public Uri buildDesktopPermaLink(Trip t) {
		if (!t.isValid()) {
			throw new IllegalStateException("Can't form URI");
		}
		StringBuffer b = new StringBuffer("https://www.tripwatcher.com/#/flights/results?");
		b.append("origin=" + t.org);
		if (!TextUtils.isEmpty(t.dst)) {
			b.append("&destination="+ t.dst);
		}

		if (t.isWeekend) {
			b.append("&period=weekend");
		} else if (t.months != null && t.months.length > 0) {
			// just pick first one in the list
			int twMonth = getTwMonth(t.months[0]);
			if (twMonth > 0) {
				b.append("&period=month");
				b.append("&month="+twMonth);
			}
		} else if (t.departing != null && t.returning != null) {
			b.append("&period=date");
			b.append("&departureDate="+ t.departing);
			b.append("&returnDate="+ t.returning);
		}
		return Uri.parse(b.toString());
	}

	private int getTwMonth(String mon) {
		Month m = Month.valueOf(mon);
		if (m != null) {
			return m.calMonth+1; // calMonth starts at 0. TripWatcher expects Jan to be "1"
		}
		return -1;
	}

	//
	// Helpers
	//

	private Uri buildQueryUrl(Trip trip) {
		Uri.Builder b = BASE_URI.buildUpon();
		if (!TextUtils.isEmpty(trip.dst)) {
			b.appendQueryParameter("destination", trip.dst);
		}
		if (!TextUtils.isEmpty(trip.org)) {
			b.appendQueryParameter("origin", trip.org);
		}
		if (trip.timeRanges != null && trip.timeRanges.length > 0) {
			String val = TwProvider.toCommaDelimited(trip.timeRanges);
			b.appendQueryParameter("timeranges", val);
		}
		if (trip.stopovers != null) {
			try {
				Stopover s = Stopover.valueOf(trip.stopovers);
				b.appendQueryParameter("stopovers", s.name());
			} catch (IllegalArgumentException e) {
				// couldn't resolve stopover value
			}
		}

		b.appendQueryParameter("weekends", trip.isWeekend ? "true" : "false");
		b.appendQueryParameter("flexible", trip.isFlexible ? "true" : "false");

		if (trip.months != null && trip.months.length > 0) {
			for (String m : trip.months) {
				b.appendQueryParameter("month", m);
			}
		}

		if (!TextUtils.isEmpty(trip.departing)) {
			// Assumes that departing value follows the "yyyy-MM-dd" format
			// (which it would if user used #setBy..())
			b.appendQueryParameter("departuredate", trip.departing);
		}
		if (!TextUtils.isEmpty(trip.returning)) {
			// Assumes that departing value follows the "yyyy-MM-dd" format
			// (which it would if user used #setBy..())
			b.appendQueryParameter("returndate", trip.returning);
		}
		return b.build();
	}

	private String getJsonResp(Uri uri) {
		HttpURLConnection conn = null;
		InputStream in = null;
		try {
			URL url = new URL(uri.toString());
			conn = (HttpURLConnection) url.openConnection();
			if (conn.getResponseCode() >= HttpStatus.SC_BAD_REQUEST) {
				Log.w(TAG, "Got error code " + conn.getResponseCode());
				return null;
			}
			in = new BufferedInputStream(conn.getInputStream());
			StringBuilder json = inputStreamToString(in);
			if (json == null || json.length() == 0) {
				Log.w(TAG, "Got empty json response for uri " + uri);
				return null;
			}
			Log.d(TAG, "Getting JSON at uri: " + uri + ", and got json size, "
					+ json.length());
			return json.toString();
		} catch (UnknownHostException uhe) {
			Log.w(TAG, "Network is not available", uhe);
			return null;
		} catch (IOException ie) {
			Log.w(TAG, "Potentially a 401 unauthorized error", ie);
			return null;
		} catch (Exception e) {
			Log.w(TAG, "Unable to get ", e);
			return null;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					Log.w(TAG, "Unable to close stream", e);
				}
			}
			if (conn != null) {
				conn.disconnect();
			}
		}
	}

	// Fast Implementation
	private StringBuilder inputStreamToString(InputStream is) {
		String line = "";
		StringBuilder total = new StringBuilder();

		// Wrap a BufferedReader around the InputStream
		BufferedReader rd = new BufferedReader(new InputStreamReader(is));

		// Read response until the end
		try {
			while ((line = rd.readLine()) != null) {
				total.append(line);
			}
		} catch (IOException e) {
			Log.w(TAG, "Unable to read input stream", e);
		}

		// Return full string
		return total;
	}

}

package org.chenhome.tripwatcher.client;

import java.io.Reader;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import org.chenhome.tripwatcher.data.Flight;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

/**
 * Plain-old-object encapsulating the TripWatcher's RESTFUL Api's JSON
 * response
 */
public class TwApiResp {
	public Flight[] flights;
	public int resultCount;
	
	public static TwApiResp fromJson(Reader reader) {
		GsonBuilder b = new GsonBuilder();
		b.registerTypeAdapter(Date.class, new DateSerializer());
		Gson gson = b.create();
		return gson.fromJson(reader, TwApiResp.class);
	}
	
	private static final String[] DATE_FORMATS = new String[] {
		"yyyy-MM-dd'T'HH:mm",
		"yyyy-MM-dd"
	};

	private static class DateSerializer implements JsonDeserializer<Date> {

	    @Override
	    public Date deserialize(JsonElement jsonElement, Type typeOF,
	            JsonDeserializationContext context) throws JsonParseException {
	        for (String format : DATE_FORMATS) {
	            try {
	                return new SimpleDateFormat(format, Locale.US).parse(jsonElement.getAsString());
	            } catch (ParseException e) {
	            }
	        }
	        throw new JsonParseException("Unparseable date: \"" + jsonElement.getAsString()
	                + "\". Supported formats: " + Arrays.toString(DATE_FORMATS));
	    }
	}
}
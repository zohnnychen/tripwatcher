package org.chenhome.tripwatcher;

import org.chenhome.tripwatcher.data.AirportTable;
import org.chenhome.tripwatcher.data.Trip;
import org.chenhome.tripwatcher.data.TripMgr;
import org.chenhome.tripwatcher.ui.CityCache;
import org.chenhome.tripwatcher.ui.Util;
import org.chenhome.tripwatcher.ui.Util.Font;

import android.app.Activity;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.MenuItem.OnActionExpandListener;
import android.widget.SearchView;
import android.widget.SearchView.OnCloseListener;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.SearchView.OnSuggestionListener;

/**
 * Helper for managing state and visual-state of action bar. This action bar
 * variant only has a single control, "Set Origin"
 */
public class TopDstActionBar extends BroadcastReceiver {

	protected static final String TAG = "OriginBarHlp";
	private Activity mAct;
	private TripMgr mTripMgr;
	private MenuItem mFromItem;
	private CityCache mCityCache;

	public TopDstActionBar(Activity activity, TripMgr mgr) {
		mAct = activity;
		mAct.getActionBar().setDisplayShowTitleEnabled(true);
		SpannableString styled = Util.withCustomTypeface(activity,
				activity.getString(R.string.top_dst), Font.BEBAS);
		mAct.getActionBar().setTitle(styled);
		mTripMgr = mgr;
		mCityCache = TwApp.getCityCache(activity);

		// If trip changes, update the MenuItem's label with new airport code
		LocalBroadcastManager.getInstance(activity).registerReceiver(this,
				new IntentFilter(TripMgr.ACTION_TRIP_ONCHANGE));
	}

	public void onDestroy() {
		LocalBroadcastManager.getInstance(mAct).unregisterReceiver(this);
	}

	public void onCreateOptions(final Menu menu) {
		MenuInflater inflater = mAct.getMenuInflater();
		inflater.inflate(R.menu.menu_topdst, menu);		
		mFromItem = menu.findItem(R.id.menuitem_opt_from);
		
		SearchViewListener listener = new SearchViewListener();
		mFromItem.setOnActionExpandListener(listener);
		
		SearchManager searchManager = (SearchManager) mAct
				.getSystemService(Context.SEARCH_SERVICE);
		SearchView search = (SearchView) mFromItem.getActionView();
		search.setSearchableInfo(searchManager.getSearchableInfo(mAct
				.getComponentName()));
		search.setOnQueryTextListener(listener);
		search.setOnCloseListener(listener);
		search.setOnSuggestionListener(listener);
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent != null
				&& intent.getAction().equals(TripMgr.ACTION_TRIP_ONCHANGE)) {
			// Trip state has been changed by someone, update the menu item
			// label
			if (mFromItem != null) {
				initFromMenuItem(mFromItem);
			}
		}
	}

	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem from = menu.findItem(R.id.menuitem_opt_from);
		if (from != null) {
			initFromMenuItem(from);
			return true;
		}
		return false;
	}	
	
	public void expandFromAction() {
		if (mFromItem != null) {
			mFromItem.expandActionView();
		}
	}


	private void initFromMenuItem(MenuItem item) {
		Trip trip = mTripMgr.getTrip();
		if (trip != null) {
			String code = trip.org;
			SearchView search = (SearchView) item.getActionView();
			if (TextUtils.isEmpty(code)) {
				search.setQueryHint(mAct.getString(R.string.search_hint));
			} else {
				String city = Util.getCity(code, mCityCache);
				search.setQueryHint(mAct.getString(R.string.opt_from2,
						new String[] { city }));
			}
			item.setVisible(false);
		}
	}

	private class SearchViewListener implements OnQueryTextListener,
			OnActionExpandListener, OnCloseListener, OnSuggestionListener {

		public boolean onQueryTextChange(String arg0) {
			return false;
		}

		public boolean onQueryTextSubmit(final String code) {
			Log.d(TAG, "Query:" + code);
			mFromItem.collapseActionView();

			AsyncTask.execute(new Runnable() {
				@Override
				public void run() {
					Uri airport = AirportTable.queryByAirportCode(mAct, code);
					if (airport != null) {
						mTripMgr.setOrigin(code);
					}
				}
			});

			return false;
		}

		@Override
		public boolean onMenuItemActionCollapse(MenuItem item) {
			Log.d(TAG, "Collapsed" + this);
			mFromItem.setVisible(true);
			return true;
		}

		@Override
		public boolean onMenuItemActionExpand(MenuItem item) {
			mFromItem.setVisible(false);
			return true;
		}

		@Override
		public boolean onClose() {
			mFromItem.collapseActionView();
			return true;
		}

		@Override
		public boolean onSuggestionClick(int position) {
			mFromItem.collapseActionView();
			return false;
		}

		@Override
		public boolean onSuggestionSelect(int position) {
			return false;
		}

	}

}

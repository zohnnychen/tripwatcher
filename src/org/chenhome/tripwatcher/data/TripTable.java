package org.chenhome.tripwatcher.data;

import java.util.ArrayList;

import android.net.Uri;
import edu.mit.mobile.android.content.ContentItem;
import edu.mit.mobile.android.content.ProviderUtils;
import edu.mit.mobile.android.content.UriPath;
import edu.mit.mobile.android.content.column.DBColumn;
import edu.mit.mobile.android.content.column.FloatColumn;
import edu.mit.mobile.android.content.column.IntegerColumn;
import edu.mit.mobile.android.content.column.TextColumn;
import edu.mit.mobile.android.content.column.TimestampColumn;

/**
 * Abstraction of the data available in the Provider.
 */
@UriPath(TripTable.PATH)
public class TripTable implements ContentItem {

	/**
	 * The table name offered by this provider
	 */
	public static final String PATH = "trip";

	/**
	 * The content:// style URL for this table
	 */
	public static final Uri CONTENT_URI = ProviderUtils.toContentUri(
			TwProvider.AUTHORITY, PATH);

	/**
	 * IATA 3-letter code
	 */
	@DBColumn(type = TextColumn.class)
	public static final String DST = "dst";

	/**
	 * Origin must be specified IATA 3-letter code
	 */
	@DBColumn(type = TextColumn.class, notnull = true)
	public static final String ORG = "org";

	/**
	 * Millis since the epoch
	 */
	@DBColumn(defaultValueInt = 0, type = TimestampColumn.class)
	public static final String UPDATED = "updated";

	@DBColumn(defaultValueInt = 0, type = FloatColumn.class)
	public static final String PRICE = "price";

	/**
	 * Can be positive or negative price delta from previous and current
	 * price
	 */
	@DBColumn(defaultValueInt = 0, type = FloatColumn.class)
	public static final String PRICE_DELTA = "price_delta";

	/**
	 * ISO date yyyy-MM-dd
	 */
	@DBColumn(type = TextColumn.class)
	public static final String DEPARTING = "departing";

	/**
	 * ISO date yyyy-MM-dd
	 */
	@DBColumn(type = TextColumn.class)
	public static final String RETURNING = "returning";

	@DBColumn(type = TextColumn.class)
	public static final String STOPOVERS = "stopovers";

	@DBColumn(defaultValueInt = 0, type = IntegerColumn.class)
	public static final String IS_FLEXIBLE = "is_flexible";

	@DBColumn(defaultValueInt = 0, type = IntegerColumn.class)
	public static final String IS_WEEKEND = "is_weekend";

	/**
	 * Comma-separated string of values: om, oa, oe, or, im, ia, ie, ir
	 * 
	 * <pre>
	 * om - outbound morning [06:00, 11:00)
	 * oa - outbound afternoon [11:00, 16:00)
	 * oe - outbound evening [16:00, 22:00)
	 * or - outbound redeye [22:00, 06:00)
	 * im - inbound morning [06:00, 11:00)
	 * ia - inbound afternoon [11:00, 16:00)
	 * ie - inbound evening [16:00, 22:00)
	 * ir - inbound redeye [22:00, 06:00)
	 * </pre>
	 */
	@DBColumn(type = TextColumn.class)
	public static final String TIME_RANGES = "time_ranges";

	/**
	 * Comma-delimited months, where each moth is three-letter like JAN, MAR, etc
	 */
	@DBColumn(type = TextColumn.class)
	public static final String MONTHS = "months";

	public enum TripCol {
		ID(TripTable._ID,0),
		DST(TripTable.DST,1),
		ORG(TripTable.ORG,2),
		UPDATED(TripTable.UPDATED,3),
		PRICE(TripTable.PRICE,4),
		PRICE_DELTA(TripTable.PRICE_DELTA,5),
		DEPARTING(TripTable.DEPARTING,6),
		RETURNING(TripTable.RETURNING,7),
		STOPOVERS(TripTable.STOPOVERS,8),
		IS_FLEXIBLE(TripTable.IS_FLEXIBLE,9),
		IS_WEEKEND(TripTable.IS_WEEKEND,10),
		TIME_RANGES(TripTable.TIME_RANGES,11),
		MONTHS(TripTable.MONTHS,12);
																				
		TripCol(String col, int index) {
			this.col = col;
			this.ind = index;
		}
		public String col;
		public int ind;

		/**
		 * Returns in order projection of column names
		 */
		public static String[] proj() {
			ArrayList<String> result = new ArrayList<String>();
			for (TripCol p : values()) {
				result.add(p.col);
			}
			return result.toArray(new String[] {});
		}

	}

}
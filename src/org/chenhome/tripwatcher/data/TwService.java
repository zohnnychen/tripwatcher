package org.chenhome.tripwatcher.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

import org.chenhome.tripwatcher.FlightsActivity;
import org.chenhome.tripwatcher.MyTripsActivity;
import org.chenhome.tripwatcher.R;
import org.chenhome.tripwatcher.client.TwApi;
import org.chenhome.tripwatcher.data.AirportTable.AirportCol;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentProviderResult;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;

import com.commonsware.cwac.wakeful.WakefulIntentService;
import com.google.analytics.tracking.android.EasyTracker;

public class TwService extends WakefulIntentService {
	private static final String NAME = "TwSvc";
	private static final String TAG = "TwSvc";
	private static final float METERS_CONSIDERED_NEAR = 1000 * 100; // km
	private static final String PREF_LAST_LATITUDE = "PREF_LAST_LATITUDE";
	private static final String PREF_LAST_LONGITUDE = "PREF_LAST_LONGITUDE";
	private static final int MAX_QUEUE_SIZE = 5;
	private static final String PREF_LAST_PRICECHECK_TIMESTAMP = "PREF_LAST_PRICECHECK_TIMESTAMP";
	private static final int PERCENT_POSITIVE_CHANGE_REQUIRES_NOTIFY = 10;
	private static final int NOTIFY_REQ = 105;

	public TwService() {
		super(NAME);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		EasyTracker.getInstance().setContext(this);
		Log.d(TAG, "Created");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void doWakefulWork(Intent intent) {
		Log.d(TAG, "doWork " + intent.getAction());
		onHandleIntentImpl(intent);
	}

	protected void onHandleIntentImpl(Intent intent) {
		Cmd cmd = Cmd.resolve(intent);
		if (cmd == null) {
			Log.w(TAG,
					"Unable to resolve intent to a known command, "
							+ intent.getAction());
			return;
		}
		long startTime = System.currentTimeMillis();
		Intent override = null;
		try {
			switch (cmd) {
			case SVC_LOAD_AIRPORTS:
				int numAirports = AirportLoader.SINGLETON.numAirports(this);
				if (numAirports == 0) {
					AirportLoader.SINGLETON.loadFromCsv(this);
				} else {
					Log.d(TAG,
							"Ignoring request to load airports. There are already "
									+ numAirports + " in the db");
				}
				break;
			case SVC_CHECK_MYTRIPS:
				handleCheckPrices();
				break;
			case SVC_REQUEST_LOCATION:
				handleRequestLocation();
				break;
			case SVC_RESPONSE_LOCATION:
				override = handleResponseLocation(intent);
				break;
			default:
				break;
			}
		} catch (Exception e) {
			Log.w(TAG, "Unable to handle command, " + cmd, e);
		}
		if (override == null) {
			sendBroadcast(cmd.toOnCmdFinish());
		} else {
			sendBroadcast(override);
		}
		Log.i(TAG, "Handling " + cmd + ", with intent " + intent + ". Took "
				+ (System.currentTimeMillis() - startTime) / 1000 + " seconds");

	}

	private void handleCheckPrices() {

		SharedPreferences.Editor e = getSharedPreferences(TAG,
				Context.MODE_PRIVATE).edit();
		e.putLong(PREF_LAST_PRICECHECK_TIMESTAMP, System.currentTimeMillis());
		e.commit();

		Log.i(TAG, "Checking prices at " + new Date(System.currentTimeMillis()));
		List<Trip> trips = TripMgr.getSavedTrips(this);

		List<Trip> goodDeals = new ArrayList<Trip>();
		for (Trip trip : trips) {
			if (trip != null && trip.isValid()) {
				List<Flight> flights = TwApi.SINGLETON.search(trip);
				Flight flt = getCheapest(flights);
				if (flt != null) {
					double origPrice = trip.price;

					// Positive means a price drop
					trip.priceDelta = (trip.price - flt.price) / trip.price;
					trip.price = flt.price;
					if (trip.priceDelta != 0) {
						// Round to nearest 2 decimal places
						double delta = Math.round(trip.priceDelta * 100);
						delta = delta / 100;
						if (delta != 0) {
							trip.priceDelta = delta;
							TripMgr mgr = new TripMgr(this, trip);
							mgr.updateTrip(this);
							Log.d(TAG,
									"Determined trip has changed in price to "
											+ trip.price + " from " + origPrice
											+ ", a normalized delta of "
											+ trip.priceDelta);
							int percent = (int) (trip.priceDelta * 100);
							if (percent > PERCENT_POSITIVE_CHANGE_REQUIRES_NOTIFY) {
								goodDeals.add(trip);
							}

						}
					}

				}
			}
		}
		if (goodDeals.size() > 0) {
			postGoodDealNotification(goodDeals);
		}
	}

	@SuppressWarnings("deprecation")
	private void postGoodDealNotification(List<Trip> goodDeals) {

		try {
			NotificationManager mgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
			Notification.Builder b = new Notification.Builder(this);

			// Figure out best trip first. At least one trip in the list for
			// sure
			Trip best = goodDeals.get(0);
			for (Trip t : goodDeals) {
				if (t.priceDelta > best.priceDelta) {
					best = t;
				}
			}
			Intent intent = new Intent(this, FlightsActivity.class);
			intent.putExtra(FlightsActivity.EXTRA_TRIP_JSON, best.toJson());

			PendingIntent pI = PendingIntent.getActivity(this, NOTIFY_REQ,
					intent, PendingIntent.FLAG_ONE_SHOT);

			b.setContentIntent(pI);
			b.setSmallIcon(R.drawable.ic_launcher);
			b.setWhen(System.currentTimeMillis());
			b.setContentTitle(getString(R.string.gooddeal_title));
			b.setNumber(goodDeals.size());
			b.setTicker(getString(R.string.gooddeal_title));
			b.setAutoCancel(true);
			String buf = null;
			if (goodDeals.size() == 1) {
				buf = getString(R.string.gooddeal_example2, (int) best.price,
						best.org, best.dst);
			} else {
				buf = getString(R.string.gooddeal_example, (int) best.price,
						best.org, best.dst, goodDeals.size());
			}
			if (buf != null) {
				b.setContentText(buf.toString());
			}
			Log.d(TAG, "Posting " + goodDeals.size()
					+ " good deals to notification bar: " + buf);
			mgr.notify(NOTIFY_REQ, b.getNotification());
		} catch (Exception e) {
			Log.w(TAG, "Unable to post notif");
		}
	}

	private Flight getCheapest(List<Flight> flights) {
		if (flights == null || flights.size() == 0) {
			return null;
		}
		Flight cheapestF = null;
		double cheapest = Double.MAX_VALUE;
		for (Flight f : flights) {
			if (f != null && f.price > 0 && f.price < cheapest) {
				cheapestF = f;
				cheapest = f.price;
			}
		}
		return cheapestF;
	}

	private Intent handleResponseLocation(Intent intent) {
		if (!intent.hasExtra(LocationManager.KEY_LOCATION_CHANGED)) {
			Log.w(TAG, "Can't get location. Missing location info, " + intent);
			return null;
		}
		try {
			Location loc = intent
					.getParcelableExtra(LocationManager.KEY_LOCATION_CHANGED);
			// assume loc is good. No appropriate checks available
			if (!isNearLastKnownLocation(loc)) {
				// update all proximities. VERY long operation
				long start = System.currentTimeMillis();
				updateAllProximities(loc);
				Log.d(TAG, "Took " + (System.currentTimeMillis() - start)
						/ 1000 + " seconds to update all proximities");
			}

			saveLocation(loc);

			String iataCode = queryNearestAirport();
			if (!TextUtils.isEmpty(iataCode)) {
				Intent resp = Cmd.SVC_RESPONSE_LOCATION.toOnCmdFinish();
				resp.putExtra(Cmd.EXTRA_NEAREST_AIRPORTCODE, iataCode);
				Log.d(TAG, "Determined closest airport is " + iataCode);

				return resp;
			}
			return null;
		} catch (Exception e) {
			Log.w(TAG, "Unable to get location from received intent" + intent);
			return null;
		}
	}

	/**
	 * Super expensive operation to update all proximities of airports that are
	 * near the current location.
	 * 
	 * Only update the table rows for airports that are close by. Others, leave
	 * them alone.
	 * 
	 * @param loc
	 */
	private void updateAllProximities(Location loc) {
		Cursor c = null;
		try {
			ContentValues values = new ContentValues();
			values.putNull(AirportCol.PROXIMITY_KM.col);
			int numCleared = getContentResolver().update(
					AirportTable.CONTENT_URI, values,
					AirportCol.PROXIMITY_KM + " IS NOT NULL", null);
			Log.d(TAG, "Cleared " + numCleared + " proximities");

			c = getContentResolver().query(
					AirportTable.CONTENT_URI,
					AirportCol.proj(),
					AirportCol.LATITUDE.col + " IS NOT NULL AND "
							+ AirportCol.LONGITUDE.col + " IS NOT NULL", null,
					null);
			if (c == null || c.getCount() == 0) {
				Log.d(TAG, "No airports with lats/longs available to examine");
				return;
			}

			// Head of queue is closest airport (least element in natural
			// ordering), tail of queue is farthest away
			PriorityQueue<AirportRow> closestAirports = new PriorityQueue<AirportRow>(
					3, new AirportComparator());
			final double curLat = loc.getLatitude();
			final double curLong = loc.getLongitude();
			Log.d(TAG, "Calculating proximities for " + c.getCount()
					+ " airports");
			while (c.moveToNext()) {
				double latitude = c.getFloat(AirportCol.LATITUDE.ind);
				double longitude = c.getFloat(AirportCol.LONGITUDE.ind);
				float[] results = new float[3];
				Location.distanceBetween(curLat, curLong, latitude, longitude,
						results);

				if (results[0] > 0) {
					AirportRow row = new AirportRow(
							c.getInt(AirportCol.ID.ind), (int) results[0]);
					closestAirports.add(row);
					if (closestAirports.size() > MAX_QUEUE_SIZE) {
						closestAirports.poll();
					}
				}
			}
			Log.d(TAG,
					"After calculating proximities, have "
							+ closestAirports.size() + " airport proximities");
			updateDbProximities(closestAirports);
		} catch (Exception e) {
			Log.w(TAG, "Unable to update all proximities", e);
		} finally {
			if (c != null) {
				c.close();
			}
		}

	}

	private void updateDbProximities(PriorityQueue<AirportRow> closestAirports)
			throws RemoteException, OperationApplicationException {
		if (closestAirports == null
				|| closestAirports.size() > MAX_QUEUE_SIZE * 2) {
			throw new IllegalArgumentException(
					"Expected a reasonable sized set of airports");
		}
		ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
		while (!closestAirports.isEmpty()) {
			AirportRow row = closestAirports.poll();
			if (row.isValid()) {
				Log.d(TAG, "Close airport within " + row.proximateMeters
						+ " meters");
				Builder update = ContentProviderOperation.newUpdate(ContentUris
						.withAppendedId(AirportTable.CONTENT_URI, row.rowId));
				update.withValue(AirportCol.PROXIMITY_KM.col,
						row.proximateMeters / 1000);
				ops.add(update.build());
			}
		}

		ContentProviderResult[] result = getContentResolver().applyBatch(
				TwProvider.AUTHORITY, ops);
		Log.d(TAG, "Updating " + result.length + " of " + ops.size()
				+ " airports rows with proximities");
	}

	/**
	 * Have a comparator so that PriorityQueue can order in natural order
	 */
	private static class AirportRow {
		public int proximateMeters;
		public int rowId;

		public AirportRow(int rowId, int meters) {
			this.rowId = rowId;
			this.proximateMeters = meters;
		}

		public boolean isValid() {
			return proximateMeters > 0 && rowId > 0;
		}

	}

	private static class AirportComparator implements Comparator<AirportRow> {

		@Override
		public int compare(AirportRow lhs, AirportRow rhs) {
			Integer lhsI = Integer.valueOf(lhs.proximateMeters);
			Integer rhsI = Integer.valueOf(rhs.proximateMeters);
			// reverse comparison so that greater distance is considered less
			// than a
			// a lesser distance.
			return rhsI.compareTo(lhsI);
		}
	}

	private String queryNearestAirport() {
		Cursor c = null;
		try {
			c = getContentResolver().query(
					AirportTable.CONTENT_URI,
					AirportCol.proj(),
					AirportCol.PROXIMITY_KM.col + " IS NOT NULL AND "
							+ AirportCol.PROXIMITY_KM + " > 0", null,
					AirportCol.PROXIMITY_KM.col + " ASC");
			if (c != null && c.getCount() > 0 && c.moveToNext()) {
				// get first code, sorted by proximity
				String code = c.getString(AirportCol.CODE.ind);
				if (AirportCol.isValidCode(code)) {
					return code;
				}
			}
			return null;
		} catch (Exception e) {
			Log.w(TAG, "Unable to get nearest airport", e);
			return null;
		} finally {
			if (c != null) {
				c.close();
			}
		}

	}

	private void saveLocation(Location loc) {
		SharedPreferences pref = getSharedPreferences(TAG, Context.MODE_PRIVATE);
		Editor e = pref.edit();
		e.putFloat(PREF_LAST_LATITUDE, (float) loc.getLatitude());
		e.putFloat(PREF_LAST_LONGITUDE, (float) loc.getLongitude());
		e.commit();
	}

	public static Date getLastPriceCheck(Context context) {
		SharedPreferences pref = context.getSharedPreferences(TAG,
				Context.MODE_PRIVATE);
		long time = pref.getLong(PREF_LAST_PRICECHECK_TIMESTAMP, 0);
		if (time > 0) {
			return new Date(time);
		}
		return null;
	}

	public static boolean isNearestLocationSet(Context context) {
		SharedPreferences pref = context.getSharedPreferences(TAG,
				Context.MODE_PRIVATE);
		return pref.contains(PREF_LAST_LATITUDE)
				&& pref.contains(PREF_LAST_LONGITUDE);
	}

	private boolean isNearLastKnownLocation(Location loc) {
		SharedPreferences pref = getSharedPreferences(TAG, Context.MODE_PRIVATE);
		if (!pref.contains(PREF_LAST_LATITUDE)
				|| !pref.contains(PREF_LAST_LONGITUDE)) {
			return false;
		}
		float latitude = pref.getFloat(PREF_LAST_LATITUDE, 0);
		float longitude = pref.getFloat(PREF_LAST_LONGITUDE, 0);
		if (latitude != 0 && longitude != 0) {
			try {
				float[] results = new float[3];
				Location.distanceBetween(loc.getLatitude(), loc.getLongitude(),
						latitude, longitude, results);
				Log.d(TAG,
						"Distance between last known location and current is "
								+ results[0]);
				return (results[0] > 0 && results[0] < METERS_CONSIDERED_NEAR);
			} catch (IllegalArgumentException e) {
				return false;
			}
		}
		return false;
	}

	private void handleRequestLocation() {
		try {
			LocationManager mgr = (LocationManager) getSystemService(Service.LOCATION_SERVICE);
			Criteria criteria = new Criteria();
			criteria.setAccuracy(Criteria.ACCURACY_COARSE);
			criteria.setAltitudeRequired(false);
			criteria.setBearingAccuracy(Criteria.NO_REQUIREMENT);
			criteria.setHorizontalAccuracy(Criteria.NO_REQUIREMENT);
			criteria.setPowerRequirement(Criteria.NO_REQUIREMENT);

			String prov = mgr.getBestProvider(criteria, true);
			Location location = mgr.getLastKnownLocation(prov);
			if (location != null) {
				Log.d(TAG, "Commanding svc to update airport proximities");
				Intent i = Cmd.SVC_RESPONSE_LOCATION.toStartCmd(this);
				i.putExtra(LocationManager.KEY_LOCATION_CHANGED, location);
				WakefulIntentService.sendWakefulWork(TwService.this, i);
			} else {
				Log.d(TAG, "Requesting location update");
				PendingIntent intent = PendingIntent.getService(this, 0,
						Cmd.SVC_RESPONSE_LOCATION.toStartCmd(this),
						PendingIntent.FLAG_UPDATE_CURRENT);
				mgr.requestSingleUpdate(criteria, intent);
			}

		} catch (Exception e) {
			Log.w(TAG, "Unable to request location", e);
		}
	}

	/**
	 * Possible commands to the service. Intent's action name is the canonical
	 * name of the enum
	 */
	public enum Cmd {
		SVC_LOAD_AIRPORTS, SVC_REQUEST_LOCATION, SVC_RESPONSE_LOCATION, SVC_CHECK_MYTRIPS;

		public static final String EXTRA_NEAREST_AIRPORTCODE = "EXTRA_NEAREST_AIRPORTCODE";
		Set<String> requiredExtras;

		private Cmd(String... extras) {
			requiredExtras = new HashSet<String>();
			requiredExtras.addAll(Arrays.asList(extras));
		}

		/**
		 * @return null if no Cmd could be resolved from the intent
		 */
		public static Cmd resolve(Intent intent) {
			if (intent != null && intent.getAction() != null) {
				Cmd match = Cmd.valueOf(intent.getAction());
				for (String required : match.requiredExtras) {
					if (!intent.hasExtra(required)) {
						Log.w(TAG, "Unable to resolve intent to cmd, " + match
								+ ". Intent missing extra, " + required);
						return null;
					}
				}
				return match;
			}
			return null;
		}

		/**
		 * This service's intent filters will resolve any intents with the
		 * appropriate action names to this service.
		 */
		public Intent toStartCmd(Context context) {
			Intent i = new Intent(context, TwService.class);
			i.setAction(this.name());
			return i;
		}

		/**
		 * Broadcast to communicate that this command has been executed and is
		 * finished (mostly)
		 * 
		 * @return
		 */
		public Intent toOnCmdFinish() {
			Intent i = new Intent(Cmd.class.getPackage().getName() + "."
					+ name());
			return i;
		}
	}

}

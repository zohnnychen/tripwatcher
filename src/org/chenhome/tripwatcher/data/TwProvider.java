package org.chenhome.tripwatcher.data;

import java.util.ArrayList;
import java.util.StringTokenizer;

import org.chenhome.tripwatcher.R;

import android.app.SearchManager;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MatrixCursor.RowBuilder;
import android.net.Uri;
import android.util.Log;
import edu.mit.mobile.android.content.GenericDBHelper;
import edu.mit.mobile.android.content.QuerystringWrapper;
import edu.mit.mobile.android.content.SimpleContentProvider;

public class TwProvider extends SimpleContentProvider {

	private static final int VERSION = 1;
	public static final String AUTHORITY = "org.chenhome.tripwatcher";
	private static final int MATCH_SEARCH = 100;
	private static final String TAG = "TwProv";

	private static final String[] sExtraProjection = new String[] { AirportTable.PROXIMITY_KM, AirportTable.CODE };
	private static final String[] sResultProj = new String[] {
			AirportTable._ID, SearchManager.SUGGEST_COLUMN_ICON_2,
			SearchManager.SUGGEST_COLUMN_INTENT_ACTION,
			SearchManager.SUGGEST_COLUMN_INTENT_DATA,
			SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID,
			SearchManager.SUGGEST_COLUMN_LAST_ACCESS_HINT,
			SearchManager.SUGGEST_COLUMN_TEXT_1,
			SearchManager.SUGGEST_COLUMN_TEXT_2 };

	public TwProvider() {
		super(AUTHORITY, VERSION);
		final GenericDBHelper tripHelper = new GenericDBHelper(TripTable.class);
		final QuerystringWrapper tripWrapper = new QuerystringWrapper(
				tripHelper);
		addDirAndItemUri(tripWrapper, TripTable.PATH);

		final GenericDBHelper airportHelper = new GenericDBHelper(
				AirportTable.class);
		final QuerystringWrapper airportWrapper = new QuerystringWrapper(
				airportHelper);
		addDirAndItemUri(airportWrapper, AirportTable.PATH);

		// Search DB helper
		TwSearchDBHelper searchHelper = new TwSearchDBHelper();
		searchHelper.registerDBHelper(airportHelper, AirportTable.CONTENT_URI,
				AirportTable.NAME, AirportTable.CITY, sExtraProjection,
				new String[] { AirportTable.NAME, AirportTable.CITY,
						AirportTable.CODE });
		addSearchUri(searchHelper, null);
	}

	private static UriMatcher sMatcher = new UriMatcher(UriMatcher.NO_MATCH);
	{
		sMatcher.addURI(TwProvider.AUTHORITY,
				SearchManager.SUGGEST_URI_PATH_QUERY + "/*", MATCH_SEARCH);
		sMatcher.addURI(TwProvider.AUTHORITY,
				SearchManager.SUGGEST_URI_PATH_QUERY, MATCH_SEARCH);
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		int match = sMatcher.match(uri);
		Log.d(TAG, "Looking at uri:: " + uri + ", match: " + match);
		if (match == MATCH_SEARCH) {
			// intercept
			Log.d(TAG, "Search");
			return handleSearch(uri, projection, selection, selectionArgs,
					sortOrder);
		}
		return super
				.query(uri, projection, selection, selectionArgs, sortOrder);
	}

	private Cursor handleSearch(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		try {
			// Override sort order
			Cursor rawResult = super.query(uri, projection, selection,
					selectionArgs, AirportTable.PROXIMITY_KM + " DESC, "
							+ SearchManager.SUGGEST_COLUMN_TEXT_1 + " ASC");
			projection = projection == null ? sResultProj : projection;
			MatrixCursor result = new MatrixCursor(projection);
			while (rawResult.moveToNext()) {
				RowBuilder builder = result.newRow();
				buildRow(projection, builder, rawResult);
			}
			return result;
		} catch (Exception e) {
			Log.w(TAG, "Unable to query " + uri, e);
			return null;
		}
	}

	private void buildRow(String[] projection, RowBuilder builder, Cursor orig) {
		for (String col : projection) {
			if (col.equals(AirportTable._ID)) {
				builder.add(orig.getLong(orig.getColumnIndex(AirportTable._ID)));
			} else if (col.equals(SearchManager.SUGGEST_COLUMN_TEXT_1)) {
				String airport = orig.getString(orig.getColumnIndex(SearchManager.SUGGEST_COLUMN_TEXT_1));
				String code = orig.getString(orig.getColumnIndex(AirportTable.CODE));
				String colText1 = airport + " (" + code + ")";
				builder.add(colText1);
			} else if (col.equals(SearchManager.SUGGEST_COLUMN_TEXT_2)) {
				builder.add(orig.getString(orig
						.getColumnIndex(SearchManager.SUGGEST_COLUMN_TEXT_2)));
			} else if (col.equals(SearchManager.SUGGEST_COLUMN_INTENT_DATA)) {
				builder.add(orig.getString(orig
						.getColumnIndex(SearchManager.SUGGEST_COLUMN_INTENT_DATA)));
			} else if (col.equals(SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID)) {
				builder.add(orig.getLong(orig
						.getColumnIndex(SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID)));
			} else if (col.equals(SearchManager.SUGGEST_COLUMN_ICON_2)) {
				int proximity = orig.getInt(orig
						.getColumnIndex(AirportTable.PROXIMITY_KM));
				if (proximity > 0) {
					builder.add(R.drawable.ic_nearby);
				} else {
					builder.add(0);
				}
			} else {
				builder.add(null);
			}
		}
	}

	public static String toCommaDelimited(String[] strs) {
		StringBuffer b = new StringBuffer();
		for (int i = 0; i < strs.length; i++) {
			b.append(strs[i]);
			if (i < strs.length - 1) {
				b.append(",");
			}
		}
		return b.toString();
	}

	public static String[] fromCommaDelimited(String src) {
		ArrayList<String> tags = new ArrayList<String>();
		StringTokenizer tokenizer = new StringTokenizer(src, ",");
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			tags.add(token);
		}
		return tags.toArray(new String[0]);
	}
}

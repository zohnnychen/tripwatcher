package org.chenhome.tripwatcher.data;

import org.chenhome.tripwatcher.data.TwService.Cmd;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.SystemClock;

import com.commonsware.cwac.wakeful.WakefulIntentService;

public class TwAlarmListener implements WakefulIntentService.AlarmListener {

	private static long INTERVAL = AlarmManager.INTERVAL_HALF_DAY;
	//private static long INTERVAL = 30000;
	// Needed by supporting library
	public TwAlarmListener() {
	}
	
	@Override
	public long getMaxAge() {
		return INTERVAL;
	}

	@Override
	public void scheduleAlarms(AlarmManager mgr, PendingIntent op,
			Context context) {
		mgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
				SystemClock.elapsedRealtime(), INTERVAL, op);
				
	}

	@Override
	public void sendWakefulWork(Context context) {
		WakefulIntentService.sendWakefulWork(context, Cmd.SVC_CHECK_MYTRIPS.toStartCmd(context));
	}
	
}
package org.chenhome.tripwatcher.data;

import java.util.ArrayList;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import edu.mit.mobile.android.content.ContentItem;
import edu.mit.mobile.android.content.ProviderUtils;
import edu.mit.mobile.android.content.UriPath;
import edu.mit.mobile.android.content.column.DBColumn;
import edu.mit.mobile.android.content.column.DoubleColumn;
import edu.mit.mobile.android.content.column.IntegerColumn;
import edu.mit.mobile.android.content.column.TextColumn;

/**
 * Single airport
 */
@UriPath(AirportTable.PATH)
public class AirportTable implements ContentItem {
	/**
	 * The table name offered by this provider
	 */
	static final String PATH = "airport";

	/**
	 * The content:// style URL for this table
	 */
	public static final Uri CONTENT_URI = ProviderUtils.toContentUri(
			TwProvider.AUTHORITY, PATH);

	/**
	 * in en-US
	 */
	@DBColumn(type = TextColumn.class)
	public static final String NAME = "name";

	/**
	 * in en-US
	 */
	@DBColumn(type = TextColumn.class)
	public static final String CITY = "city";

	/**
	 * IATA airport code
	 */
	@DBColumn(type = TextColumn.class)
	public static final String CODE = "code";

	@DBColumn(type = DoubleColumn.class)
	public static final String LATITUDE = "latitude";

	@DBColumn(type = DoubleColumn.class)
	public static final String LONGITUDE = "longitude";

	@DBColumn(type = IntegerColumn.class)
	public static final String PROXIMITY_KM = "proximity_km";

	@DBColumn(type = IntegerColumn.class)
	public static final String IS_RECENT = "is_recent";

	/**
	 * Probably in the assets folder and thus of the form,
	 * file:///android_asset/..."
	 */
	@DBColumn(type = TextColumn.class)
	public static final String IMAGE_URI = "image_uri";

	private static final String TAG = "AirportTable";

	public enum AirportCol {
		ID(AirportTable._ID, 0), NAME(AirportTable.NAME, 1), CITY(
				AirportTable.CITY, 2), CODE(AirportTable.CODE, 3), LATITUDE(
				AirportTable.LATITUDE, 4), LONGITUDE(AirportTable.LONGITUDE, 5), PROXIMITY_KM(
				AirportTable.PROXIMITY_KM, 6), IS_RECENT(
				AirportTable.IS_RECENT, 7), IMAGE_URI(AirportTable.IMAGE_URI, 8);

		AirportCol(String col, int index) {
			this.col = col;
			this.ind = index;
		}

		public String col;
		public int ind;

		/**
		 * Returns in order projection of column names
		 */
		public static String[] proj() {
			ArrayList<String> result = new ArrayList<String>();
			for (AirportCol p : values()) {
				result.add(p.col);
			}
			return result.toArray(new String[] {});
		}

		public static boolean isValidCode(String code) {
			return (!TextUtils.isEmpty(code) && code.length() == 3);
		}

	}

	/**
	 * @param code
	 * @return null if no single airport found matchingt the 3-letter IATA code.
	 *         Else the uri to the matching airport
	 */
	public static Uri queryByAirportCode(Context context, String code) {
		if (!AirportCol.isValidCode(code)) {
			return null;
		}
		Cursor c = null;
		try {
			// "LIKE" should be case insensitive
			c = context.getContentResolver().query(AirportTable.CONTENT_URI, AirportCol.proj(),
					AirportTable.CODE + " like ?", new String[] {code.trim()}, null);
			if (c != null && c.getCount() == 1 && c.moveToNext()) {
				Uri result = ContentUris.withAppendedId(AirportTable.CONTENT_URI, c.getInt(AirportCol.ID.ind));
				Log.d(TAG,"For code " + code + ", found airport " + result);
				return result;
			}
			return null;
		} catch (Exception e) {
			Log.w(TAG, "Unable to get airport matching code " + code, e);
			return null;
		} finally {
			if (c != null)
				c.close();
		}
	}

	public static String queryCityByAirportCode(Context context, String code) {
		if (!AirportCol.isValidCode(code)) {
			return null;
		}
		Cursor c = null;
		try {
			// "LIKE" should be case insensitive
			c = context.getContentResolver().query(AirportTable.CONTENT_URI, AirportCol.proj(),
					AirportTable.CODE + " like ?", new String[] {code.trim()}, null);
			if (c != null && c.getCount() == 1 && c.moveToNext()) {
				return c.getString(AirportCol.CITY.ind);
			}
			return null;
		} catch (Exception e) {
			Log.w(TAG, "Unable to get airport matching code " + code, e);
			return null;
		} finally {
			if (c != null)
				c.close();
		}
	}
	/**
	 * @param airport
	 * @return 3-letter IATA code matching the argument Airport URI. Null if none can be found
	 */
	public static String getCodeByAirportUri(Context context, Uri airport) {
		Cursor c = null;
		try {
			c = context.getContentResolver().query(airport, AirportCol.proj(),
					null, null, null);
			if (c != null && c.moveToNext()) {
				String code = c.getString(AirportCol.CODE.ind);
				if (!TextUtils.isEmpty(code) && AirportCol.isValidCode(code)) {
					Log.d(TAG,"For airport " + airport + ", found code " + code);
					return code;
				}
			}
			return null;
		} catch (Exception e) {
			Log.w(TAG, "Unable to get airport " + airport, e);
			return null;
		} finally {
			if (c != null)
				c.close();
		}
	}
}
package org.chenhome.tripwatcher.data;

import java.util.Date;

import com.google.gson.Gson;

public class Flight {

	public String origin;
	public String destination;
	public double price;
	public String link;
	public Outbound outbound;
	public Inbound inbound;

	public static class Outbound {
		public String carrier;
		public String flightNumber;
		public Date departure;
		public Date arrival;
		public int durationMinutes;
		public int stopoverCount;
		public String[] stopovers;
	}

	public static class Inbound {
		public String carrier;
		public String flightNumber;
		public Date departure;
		public Date arrival;
		public int durationMinutes;
		public int stopoverCount;
		public String[] stopovers;
	}
	
	public static Flight instantiate(String jsonStr) {
		Gson gson = new Gson();
		return gson.fromJson(jsonStr, Flight.class);
	}

	public String toJsonString() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}
	
}

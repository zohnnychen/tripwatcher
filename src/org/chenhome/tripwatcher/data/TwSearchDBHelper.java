package org.chenhome.tripwatcher.data;

import java.util.ArrayList;
import java.util.LinkedList;

import org.chenhome.tripwatcher.BuildConfig;

import android.annotation.TargetApi;
import android.app.SearchManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import edu.mit.mobile.android.content.ContentItem;
import edu.mit.mobile.android.content.GenericDBHelper;
import edu.mit.mobile.android.content.ProviderUtils;
import edu.mit.mobile.android.content.dbhelper.SearchDBHelper;

/**
 * Extends DbHelper from Steve P.
 */
public class TwSearchDBHelper extends SearchDBHelper {

	private static final String TAG = "TwSearchDBHelper";
	LinkedList<RegisteredHelper> mRegisteredHelpers = new LinkedList<TwSearchDBHelper.RegisteredHelper>();

	public TwSearchDBHelper() {

	}

	/**
	 * <p>
	 * Adds a {@link GenericDBHelper} to the list of search helpers that will be
	 * queried for this search. All the registered DBHelpers will be searched
	 * and results will be mixed together.
	 * </p>
	 * 
	 * <p>
	 * The columns to search, provided in {@code searchColumns}, will be queried
	 * using a simple {@code LIKE "%query%"} substring search. The results are
	 * concatenate using {@code UNION ALL}.
	 * </p>
	 * 
	 * @param helper
	 *            the helper you wish to search. This must return a valid result
	 *            for {@link GenericDBHelper#getTable()}.
	 * @param contentUri
	 *            the base URI that will be used when linking back to the item
	 *            from the search results (see
	 *            {@link SearchManager#SUGGEST_COLUMN_INTENT_DATA}). This can be
	 *            null to disable this feature.
	 * @param text1Column
	 *            the text column that will be used for
	 *            {@link SearchManager#SUGGEST_COLUMN_TEXT_1}. This is required.
	 * @param text2Column
	 *            the text column that will be used for
	 *            {@link SearchManager#SUGGEST_COLUMN_TEXT_2}. This is optional
	 *            and can be null.
	 * @param searchColumns
	 *            a list of the columns that will be searched for the given
	 *            keyword.
	 * @param extraProjCols
	 *            a list of columns to add to the projection of the search
	 *            results
	 * 
	 */
	public void registerDBHelper(GenericDBHelper helper, Uri contentUri,
			String text1Column, String text2Column, String[] extraProjCols,
			String... searchColumns) {
		mRegisteredHelpers.add(new RegisteredHelper(helper, contentUri,
				text1Column, text2Column, extraProjCols, searchColumns));
	}

	private static class RegisteredHelper {
		public final GenericDBHelper mHelper;
		public final String[] mColumns;
		public final String mText2Column;
		public final String mText1Column;
		public final Uri mContentUri;
		private String[] mExtraProjCols;

		public RegisteredHelper(GenericDBHelper helper, Uri contentUri,
				String text1Column, String text2Column, String[] extraProjCols, String... columns) {
			mHelper = helper;
			mColumns = columns;
			mContentUri = contentUri;
			mText1Column = text1Column;
			mText2Column = text2Column;
			mExtraProjCols = extraProjCols;
		}
	}

	@Override
	public Cursor queryDir(SQLiteDatabase db, Uri uri, String[] projection,
			String selection, String[] selectionArgs, String sortOrder) {
		return search(db, uri, projection, selection, selectionArgs, sortOrder,
				true);
	}

	@Override
	public Cursor queryItem(SQLiteDatabase db, Uri uri, String[] projection,
			String selection, String[] selectionArgs, String sortOrder) {
		return search(db, uri, projection, selection, selectionArgs, sortOrder,
				false);
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressWarnings("deprecation")
	private Cursor search(SQLiteDatabase db, Uri uri, String[] projection,
			String selection, String[] selectionArgs, String sortOrder,
			boolean isDir) {

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "search(" + uri + ")");
		}
		final String searchQuery = isDir ? null : "%"
				+ uri.getLastPathSegment() + "%";

		String limit = uri.getQueryParameter("limit");

		// only allow a limit string that's an integer
		try {
			Integer.valueOf(limit);
		} catch (final NumberFormatException e) {
			limit = null;
		}

		final StringBuilder multiSelect = new StringBuilder();

		multiSelect.append('(');

		final SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

		boolean addUnion = false;
		for (final RegisteredHelper searchReg : mRegisteredHelpers) {

			// UNION ALL concatenates the inner queries
			if (addUnion) {
				multiSelect.append(" UNION ALL ");
			}
			addUnion = true;

			String searchSelection = null;

			if (!isDir) {
				final StringBuilder extSel = new StringBuilder();
				// build the selection that matches the search string in the
				// given
				int i = 0;
				for (final String column : searchReg.mColumns) {
					if (i > 0) {
						extSel.append(" OR ");
					}

					extSel.append("\"");
					extSel.append(column);
					extSel.append("\" LIKE ?1");

					i++;
				}
				searchSelection = extSel.toString();
			}

			final ArrayList<String> extProj = new ArrayList<String>();
			final String table = searchReg.mHelper.getTable();
			final String tablePrefix = '"' + table + "\".";

			extProj.add(tablePrefix + ContentItem._ID + " AS "
					+ ContentItem._ID);

			extProj.add(tablePrefix + searchReg.mText1Column + " AS "
					+ SearchManager.SUGGEST_COLUMN_TEXT_1);

			if (searchReg.mText2Column != null) {
				extProj.add(tablePrefix + searchReg.mText2Column + "  AS "
						+ SearchManager.SUGGEST_COLUMN_TEXT_2);
			} else {
				// this is needed as sqlite3 crashes otherwise.
				extProj.add("'' AS " + SearchManager.SUGGEST_COLUMN_TEXT_2);
			}

			if (searchReg.mContentUri != null) {
				extProj.add("'" + searchReg.mContentUri.toString() + "' AS "
						+ SearchManager.SUGGEST_COLUMN_INTENT_DATA);
				extProj.add(tablePrefix + ContentItem._ID + " AS "
						+ SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID);
			} else {
				// this is needed as sqlite3 crashes otherwise.
				extProj.add("'' AS " + SearchManager.SUGGEST_COLUMN_INTENT_DATA);
				extProj.add("'' AS "
						+ SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID);
			}
			if (searchReg.mExtraProjCols != null) {
				for (String col: searchReg.mExtraProjCols) {
					extProj.add(col);
				}
			}

			qb.setTables(table);

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				multiSelect.append(qb.buildQuery(
						extProj.toArray(new String[extProj.size()]),
						searchSelection != null ? ProviderUtils.addExtraWhere(
								selection, searchSelection) : selection, null,
						null, sortOrder, null));
			} else {
				multiSelect.append(qb.buildQuery(
						extProj.toArray(new String[extProj.size()]),
						searchSelection != null ? ProviderUtils.addExtraWhere(
								selection, searchSelection) : selection, null,
						null, null, sortOrder, null));
			}

		} // inner selects

		multiSelect.append(')');

		final Cursor c = db.query(multiSelect.toString(), null, null,
				searchQuery != null ? new String[] { searchQuery } : null,
				null, null, sortOrder, limit);
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "search found " + c.getCount() + " results");
		}
		return c;
	}

}

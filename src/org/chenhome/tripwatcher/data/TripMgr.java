package org.chenhome.tripwatcher.data;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.chenhome.tripwatcher.data.AirportTable.AirportCol;
import org.chenhome.tripwatcher.data.Trip.Month;
import org.chenhome.tripwatcher.data.Trip.Stopover;
import org.chenhome.tripwatcher.data.TripTable.TripCol;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

/**
 * Manager of the Trip criteria state.
 * <p>
 * Receives intent broadcasts and changes the state of the Trip instance.
 * <p>
 * Publishes any changes that occur to this Trip's state. All changes to a trip
 * instance should ideally happen through this manager. It can then inform any
 * interested observers
 */
public class TripMgr {
	public static final String ACTION_TRIP_ONCHANGE = "ACTION_TRIP_ONCHANGE";
	private static SimpleDateFormat sDateF = new SimpleDateFormat("EEE, MMM d",
			Locale.getDefault());

	private static final String TAG = "TripMgr";

	private Trip mTrip;
	private boolean mIsDirty;

	private LocalBroadcastManager mBroadcastMgr;

	public TripMgr(Context context, Trip trip) {
		mTrip = trip;
		mBroadcastMgr = LocalBroadcastManager.getInstance(context);
		mIsDirty = false;
	}

	public void clear() {
		// Copy origin from original trip
		String origin = mTrip.org;
		String dst = mTrip.dst;
		mTrip = new Trip();
		if (!TextUtils.isEmpty(origin)) {
			mTrip.org = origin;
		}
		// Clear the destination
//		if (!TextUtils.isEmpty(dst)) {
//			mTrip.dst = dst;
//		}
		mIsDirty = false;
		Log.d(TAG, "Clearing trip mgr");
	}

	public void setOrigin(String org) {
		Log.d(TAG, "setOrg");
		if (AirportCol.isValidCode(org)) {
			mTrip.org = normalize(org);
			notifyChange();
		}
	}

	public void setDst(String dst) {
		Log.d(TAG, "setDst");
		if (AirportCol.isValidCode(dst)) {
			mTrip.dst = normalize(dst);
			notifyChange();
			mIsDirty = true;
		}
	}

	public void setStopover(Stopover stopover) {
		mTrip.setStopover(stopover);
		notifyChange();
		mIsDirty = true;
	}

	public void setByMonth(List<Month> inMonths) {
		mTrip.setByMonth(inMonths);
		notifyChange();
		mIsDirty = true;
	}

	public Month getSelectedMonth() {
		if (mTrip.months != null && mTrip.months.length == 1
				&& !TextUtils.isEmpty(mTrip.months[0])) {
			return Month.valueOf(mTrip.months[0]);
		}
		return null;
	}

	public void setByWeekends() {
		// figure out next 5 months
		List<Month> nextMonths = Month.nextXMonths(5);
		Log.d(TAG, "Setting next 5 months: " + nextMonths);
		mTrip.setByWeekends(nextMonths);
		notifyChange();
		mIsDirty = true;
	}

	public boolean isWeekendSet() {
		return mTrip.months != null && mTrip.months.length > 0
				&& mTrip.isWeekend;
	}

	public void setByDates(Date departing, Date returning, boolean isFlexible) {
		mTrip.setByDates(departing, returning, isFlexible);
		notifyChange();
		mIsDirty = true;
	}

	public boolean isDatesSet() {
		return !TextUtils.isEmpty(mTrip.departing)
				&& !TextUtils.isEmpty(mTrip.returning);
	}

	public Trip getTrip() {
		return mTrip;
	}

	public void notifyChange() {
		Log.d(TAG, "Broadcasting trip has changed");
		Intent intent = new Intent(ACTION_TRIP_ONCHANGE);
		mBroadcastMgr.sendBroadcast(intent);
	}

	private String normalize(String dst) {
		return dst.toUpperCase();
	}

	public boolean isDirty() {
		return mIsDirty;
	}

	public boolean hasCriteriaSet() {
		boolean res = mTrip.hasClearableCriteriaSet();
		Log.d(TAG, "isCriteriaSet? " + res);
		return res;
	}

	public String getDisplayDeparting() {
		Date date = mTrip.getDeparting();
		if (date != null) {
			return sDateF.format(date);
		}
		return null;
	}

	public String getDisplayReturning() {
		Date date = mTrip.getReturning();
		if (date != null) {
			return sDateF.format(date);
		}
		return null;
	}
	
	public static List<Trip> getSavedTrips(Context context) {
		Cursor c = null;
		List<Trip> res = new ArrayList<Trip>();
		try {
			c = context.getContentResolver().query(TripTable.CONTENT_URI, TripCol.proj(), null, null, null);
			while (c.moveToNext()) {
				res.add(Trip.instantiate(c));
			}
			Log.d(TAG,"Found " + res.size() + " saved trips");
			return res;
		} finally {
			if (c != null) {
				c.close();
			}
		}
	}
	
	public void stopWatchingTrip(Context context) {
		if (mTrip == null || !mTrip.isValid()) {
			return;
		}
		Cursor c = null;
		List<Trip> res = new ArrayList<Trip>();
		int targetHash = mTrip.toUniqueHash();
		try {
			c = context.getContentResolver().query(TripTable.CONTENT_URI, TripCol.proj(), null, null, null);
			while (c.moveToNext()) {
				Trip t = Trip.instantiate(c);
				if (t != null && t.isValid()){
					if (t.toUniqueHash() == targetHash) {
						Log.d(TAG,"Found matching trip. Deleting it.");
						int rowId = c.getInt(TripCol.ID.ind);
						Uri uri = ContentUris.withAppendedId(TripTable.CONTENT_URI, rowId);
						if (context.getContentResolver().delete(uri, null, null) == 0) {
							Log.w(TAG,"Unable to delete " + uri);
						}
						return;
					}
				}
			}
			Log.d(TAG,"Found no matches among " + res.size() + " saved trips. Did not delete trip");
		} finally {
			if (c != null) {
				c.close();
			}
		}
	}
	
	public boolean isTripAlreadySaved(Context context) {
		List<Trip> saved = getSavedTrips(context);
		for (Trip save:saved) {
			if (Trip.equals(mTrip, save)) {
				return true;
			}
		}
		return false;
	}

	public void insertTrip(Context context) {
		try {
			if (mTrip != null && mTrip.isValid()) {
				Uri uri = context.getContentResolver().insert(TripTable.CONTENT_URI,
						mTrip.toValues());
				if (uri == null) {
					Log.w(TAG, "Unable to insert trip to db");
				} else {
					Log.d(TAG, "Successfully insert trip, " + uri);
				}
			}
		} catch (Exception e) {
			Log.w(TAG, "Unable to save trip to db", e);
		}
	}

	public void updateTrip(Context context) {
		try {
			if (mTrip != null && mTrip.isValid() && mTrip.rowId != 0) {
				Uri uri = ContentUris.withAppendedId(TripTable.CONTENT_URI, mTrip.rowId);
				int numUpdated = context.getContentResolver().update(uri, mTrip.toValues(), null, null);
				if (numUpdated == 0) {
					Log.w(TAG, "Unable to update trip to db");
				} else {
					Log.d(TAG, "Successfully saved trip, " + uri);
				}
			}
		} catch (Exception e) {
			Log.w(TAG, "Unable to save trip to db", e);
		}
	}
}
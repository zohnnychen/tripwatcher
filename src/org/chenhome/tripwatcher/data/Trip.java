package org.chenhome.tripwatcher.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.chenhome.tripwatcher.R;
import org.chenhome.tripwatcher.client.TwApi;
import org.chenhome.tripwatcher.data.TripTable.TripCol;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.ShareCompat;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;

public class Trip {

	// For GSON
	public String dst;
	public String org;
	public long updated;
	public double price;
	public double priceDelta;
	public String departing; // keep as String to avoid timezone-issues
	public String returning;
	public String stopovers;
	public boolean isFlexible = false; // default to false, per the API behavior
	public boolean isWeekend = false; // default to false
	public String[] timeRanges = new String[0];
	public String[] months = new String[0];
	public long rowId;

	public String toString() {
		return toJson();
	}

	public void setStopover(Stopover stopover) {
		this.stopovers = stopover.name();
	}

	public void setByMonth(List<Month> inMonths) {
		clearDates();
		this.months = new String[inMonths.size()];
		for (int i = 0; i < inMonths.size(); i++) {
			this.months[i] = inMonths.get(i).name();
		}
	}

	/**
	 * 
	 * @param inMonths
	 *            can be null to signify weekends for all months
	 */
	public void setByWeekends(List<Month> inMonths) {
		clearDates();
		// clears date
		if (inMonths != null) {
			setByMonth(inMonths);
		}
		this.isWeekend = true;
	}

	private static final SimpleDateFormat sF = new SimpleDateFormat(
			"yyyy-MM-dd");
	private static final String TAG = "Trip";

	/**
	 * @param departing
	 *            must be set
	 * @param returning
	 *            can be null
	 * @param isFlexible
	 *            defaults to false if not set
	 */
	public void setByDates(Date departing, Date returning, boolean isFlexible) {
		clearDates();
		if (departing == null || returning == null) {
			throw new IllegalArgumentException("Departing date not set");
		}
		if (returning.before(departing)) {
			throw new IllegalArgumentException("Depart date after return date");
		}
		this.departing = sF.format(departing);
		if (returning != null) {
			this.returning = sF.format(returning);
		}
		if (isFlexible) {
			this.isFlexible = isFlexible;
		} // else let it stay inflexible
	}

	public Date getDeparting() {
		return getDate(departing);
	}

	private Date getDate(String dateStr) {
		try {
			if (!TextUtils.isEmpty(dateStr)) {
				return sF.parse(dateStr);
			}
			return null;
		} catch (ParseException e) {
			return null;
		}
	}

	public Date getReturning() {
		return getDate(returning);
	}

	public enum Month {
		JAN(Calendar.JANUARY), FEB(Calendar.FEBRUARY), MAR(Calendar.MARCH), APR(
				Calendar.APRIL), MAY(Calendar.MAY), JUN(Calendar.JUNE), JUL(
				Calendar.JULY), AUG(Calendar.AUGUST), SEP(Calendar.SEPTEMBER), OCT(
				Calendar.OCTOBER), NOV(Calendar.NOVEMBER), DEC(
				Calendar.DECEMBER);
		private Month(int calMonth) {
			this.calMonth = calMonth;
		}

		public int calMonth;

		/**
		 * @param calMonth
		 *            the value from {@link Calendar.get(Calendar.MONTH)}
		 * @return null if none can be resolved
		 */
		public static Month fromCalMonth(int calMonth) {
			for (Month m : Month.values()) {
				if (m.calMonth == calMonth) {
					return m;
				}
			}
			return null;
		}

		public static List<Month> nextXMonths(int x) {
			List<Month> mths = new ArrayList<Month>();
			Calendar now = Calendar.getInstance();
			for (int i = 0; i < x; i++) {
				Month m = Month.fromCalMonth(now.get(Calendar.MONTH));
				if (m != null) {
					mths.add(m);
				}
				now.roll(Calendar.MONTH, true);
			}
			return mths;
		}

		public static Month currentMonth() {
			Calendar now = Calendar.getInstance();
			return fromCalMonth(now.get(Calendar.MONTH));
		}

		public String getDisplayMonth(Context context) {
			String[] strs = context.getResources().getStringArray(
					R.array.months);
			int ordinal = ordinal();
			if (ordinal < strs.length) {
				return strs[ordinal()];
			}
			return null;
		}

		public static Month fromOrdinal(int which) {
			return fromCalMonth(which);
		}
	}

	/**
	 * Possible values for elements in {@link Trip#timeRanges}
	 */
	public enum Range {
		// names match the value expected by the Tw server API
		om, oa, oe, or, im, ia, ie, ir;
	}

	public enum Stopover {
		// names match the value expected by the Tw server API
		any, nonstop, up_to_one;
	}

	public ContentValues toValues() {
		ContentValues v = new ContentValues();
		v.put(TripTable.DST, dst);
		v.put(TripTable.ORG, org);
		v.put(TripTable.UPDATED, updated);
		v.put(TripTable.PRICE, price);
		v.put(TripTable.PRICE_DELTA, priceDelta);
		v.put(TripTable.DEPARTING, departing);
		v.put(TripTable.RETURNING, returning);
		v.put(TripTable.STOPOVERS, stopovers);
		v.put(TripTable.IS_FLEXIBLE, isFlexible);
		v.put(TripTable.IS_WEEKEND, isWeekend);
		v.put(TripTable.TIME_RANGES, TwProvider.toCommaDelimited(timeRanges));
		v.put(TripTable.MONTHS, TwProvider.toCommaDelimited(months));
		return v;
	}

	public static Trip instantiate(String jsonStr) {
		Gson gson = new Gson();
		return gson.fromJson(jsonStr, Trip.class);
	}

	public static boolean equals(Trip t1, Trip t2) {
		// Use JSON serialized form to do comparison
		int s1 = t1.toUniqueHash();
		int s2 = t2.toUniqueHash();
		return s1 == s2;
	}

	public static void startShareChooser(Trip trip, Activity context) {
		ShareCompat.IntentBuilder b = ShareCompat.IntentBuilder.from(context);
		b.setChooserTitle(R.string.how_share_deal);
		Uri data = TwApi.SINGLETON.buildDesktopPermaLink(trip);
		b.setText(context.getString(R.string.check_out_great_deal) + " "
				+ data.toString());
		b.setType("text/plain");
		Log.d(TAG, "Got share URI " + data);

		b.startChooser();
	}

	public String toJson() {
		Gson gson = new Gson();
		return gson.toJson(this, Trip.class);
	}

	/**
	 * Unique 32-bit int for a particular serialized form of this Trip
	 * 
	 * @return
	 */
	public int toUniqueHash() {
		return toJson().hashCode();
	}

	public static Trip instantiate(Cursor c) {
		if (c == null) {
			throw new IllegalArgumentException("Invalid cursor");
		}
		Trip t = new Trip();
		t.rowId = c.getLong(TripCol.ID.ind);
		t.dst = c.getString(TripCol.DST.ind);
		t.org = c.getString(TripCol.ORG.ind);
		t.updated = c.getLong(TripCol.DST.ind);
		t.price = c.getDouble(TripCol.PRICE.ind);
		t.priceDelta = c.getDouble(TripCol.PRICE_DELTA.ind);
		t.departing = c.getString(TripCol.DEPARTING.ind);
		t.returning = c.getString(TripCol.RETURNING.ind);
		t.stopovers = c.getString(TripCol.STOPOVERS.ind);
		t.isFlexible = c.getInt(TripCol.IS_FLEXIBLE.ind) == 1;
		t.isWeekend = c.getInt(TripCol.IS_WEEKEND.ind) == 1;
		t.timeRanges = TwProvider.fromCommaDelimited(c
				.getString(TripCol.TIME_RANGES.ind));
		t.months = TwProvider.fromCommaDelimited(c
				.getString(TripCol.MONTHS.ind));
		return t;
	}

	private void clearDates() {
		months = null;
		isWeekend = false;
		isFlexible = false;
		departing = null;
		returning = null;
	}

	public boolean isValid() {
		return !TextUtils.isEmpty(org);
	}

	/**
	 * @return whether any clearable criteria have been set
	 */
	public boolean hasClearableCriteriaSet() {
		// If origin or dest set, criteria is not considered set.
		String[] strs = new String[] { departing, returning, stopovers };

		for (String s : strs) {
			if (!TextUtils.isEmpty(s)) {
				Log.d(TAG,"Clearable criteria : " + s + " was set");
				return true;
			}
		}
		boolean res = (months != null && months.length > 0) || timeRanges.length > 0;
		if (res) {
			Log.d(TAG,"Month or timerange set " + months.length + ", ranges " + timeRanges.length);
		}
		return res;
	}
}
package org.chenhome.tripwatcher.data;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.chenhome.tripwatcher.data.AirportTable.AirportCol;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;

import com.csvreader.CsvReader;

public enum AirportLoader {
	SINGLETON;

	private static final String TAG = "AirLdr";
	private static final int BATCH_LIMIT = 100;

	public static final String AIRPORT_CSVFILE = "airports.csv";

	public int numAirports(Context context) {
		Cursor c = null;
		try {
			c = context.getContentResolver().query(AirportTable.CONTENT_URI,
					new String[] { AirportCol.ID.col }, null, null, null);
			if (c != null) {
				return c.getCount();
			}
			return 0;
		} finally {
			if (c != null)
				c.close();
		}
	}

	public boolean loadFromCsv(Context context) {
		CsvReader csv = null;
		try {
			InputStreamReader in = new InputStreamReader(
					new BufferedInputStream(context.getAssets().open(
							AIRPORT_CSVFILE)));
			csv = new CsvReader(in);
			if (!csv.readHeaders()) {
				throw new Exception("Unable to read headers");
			}

			int numRecords = 0;
			int numInserted = 0;
			ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>(
					BATCH_LIMIT);

			while (csv.readRecord()) {
				numRecords++;
				Builder b = ContentProviderOperation
						.newInsert(AirportTable.CONTENT_URI);
				b.withYieldAllowed(true);

				String code = csv.get(Hdr.iataCode.name());
				if (!TextUtils.isEmpty(code)) {
					b.withValues(createAirportRow(csv));
					ops.add(b.build());
				}

				if (numRecords % BATCH_LIMIT == 0 && numRecords >= BATCH_LIMIT) {
					numInserted += insertAirports(context, ops);
					ops.clear();
				}
			}

			if (ops.size() > 0) {
				// Insert the rest
				numInserted += insertAirports(context, ops);
			}
			Log.i(TAG, "Out of " + numRecords + " csv records, inserted "
					+ numInserted);
			return true;
		} catch (Exception e) {
			Log.w(TAG, "Unable to load airports from CSV", e);
			return false;
		} finally {
			if (csv != null) {
				csv.close(); // will also close encapsulated input stream
			}
		}
	}

	private int insertAirports(Context context,
			ArrayList<ContentProviderOperation> ops) throws RemoteException,
			OperationApplicationException {
		ContentProviderResult[] results = context.getContentResolver()
				.applyBatch(TwProvider.AUTHORITY, ops);
		if (results != null) {
			return results.length;
		}
		return 0;
	}

	private ContentValues createAirportRow(CsvReader csv) throws IOException {
		ContentValues v = new ContentValues();
		Hdr[] headers = Hdr.values();
		for (Hdr header : headers) {
			String val = csv.get(header.name());
			v.put(header.col.col, val);
		}
		if (v.keySet() == null) {
			throw new IOException("Unable to read csv row. Empty row");
		}
		return v;
	}

	private enum Hdr {
		name(AirportCol.NAME), city(AirportCol.CITY), iataCode(AirportCol.CODE), latitude(
				AirportCol.LATITUDE), longitude(AirportCol.LONGITUDE);

		public AirportCol col;

		private Hdr(AirportCol col) {
			this.col = col;
		}
	}
}

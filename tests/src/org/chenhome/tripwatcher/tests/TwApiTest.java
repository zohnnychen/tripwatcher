package org.chenhome.tripwatcher.tests;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.chenhome.tripwatcher.client.TwApi;
import org.chenhome.tripwatcher.data.Flight;
import org.chenhome.tripwatcher.data.Trip;
import org.chenhome.tripwatcher.data.Trip.Month;
import org.chenhome.tripwatcher.data.Trip.Stopover;

import android.test.AndroidTestCase;

public class TwApiTest extends AndroidTestCase {

	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		super.setUp();
	}

	@Override
	protected void tearDown() throws Exception {
		// TODO Auto-generated method stub
		super.tearDown();
	}

	public void testSearch() throws ParseException {
		// test to top destinations
		Trip t1 = new Trip();
		t1.org = "SFO";
		
		List<Flight> r = TwApi.SINGLETON.search(t1);
		assertTrue(r!=null && r.size() > 0);
		for (Flight f:r) {
			assertEquals("SFO", f.origin);
		}

		// by destination
		Trip t2 = new Trip();
		t2.org = "SFO";
		t2.dst = "LAX";
		r = TwApi.SINGLETON.search(t2);
		assertTrue(r!=null && r.size() > 0);
		for (Flight f:r) {
			assertEquals("SFO", f.origin);
			assertEquals("LAX", f.destination);
		}

		// by stopover
		Trip t3 =new Trip();
		t3.org = "SFO";
		t3.setStopover(Stopover.nonstop);
		r = TwApi.SINGLETON.search(t3);
		assertTrue(r!=null && r.size() > 0);
		for (Flight f:r) {
			assertEquals(0,f.outbound.stopoverCount);
			assertEquals(0,f.inbound.stopoverCount);
		}
		
		// by weekends
		List<Month> months = new ArrayList<Month>();
		months.add(Month.SEP);
		Trip t4 = new Trip();
		t4.org ="SFO";
		t4.setByWeekends(months);
		r = TwApi.SINGLETON.search(t4);
		assertTrue(r!=null && r.size() > 0);
		for (Flight f:r) {
			int month = f.inbound.departure.getMonth();
			assertTrue(month == 9 || month == 8);
		}
		
		SimpleDateFormat sf= new SimpleDateFormat("yyyy-MM-dd");

		// by dates
		Trip t5 = new Trip();
		t5.org = "SFO";
		t5.setByDates(sf.parse("2013-10-01"), null, true);
		r = TwApi.SINGLETON.search(t5);
		assertTrue(r!=null && r.size() > 0);
		for (Flight f:r) {
			assertEquals(9, f.inbound.departure.getMonth());
		}

	}
	
}

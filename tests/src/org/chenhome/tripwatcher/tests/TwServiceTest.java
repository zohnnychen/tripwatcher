package org.chenhome.tripwatcher.tests;

import org.chenhome.tripwatcher.data.AirportTable;
import org.chenhome.tripwatcher.data.AirportTable.AirportCol;
import org.chenhome.tripwatcher.data.TripTable;
import org.chenhome.tripwatcher.data.TwProvider;
import org.chenhome.tripwatcher.data.TwService;
import org.chenhome.tripwatcher.data.TwService.Cmd;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.test.IsolatedContext;
import android.test.ServiceTestCase;
import android.test.mock.MockContentResolver;
import android.text.TextUtils;

public class TwServiceTest extends ServiceTestCase<TwService> {

	public TwServiceTest() {
		super(TwService.class);

	}

	protected void setUp() throws Exception {
		super.setUp();

		// Make PoiProvider available during testing
		MockContentResolver resolver = new MockContentResolver();

		IsolatedContext mockContext = new IsolatedContext(resolver,
				getSystemContext());

		TwProvider provider = new TwProvider();
		provider.attachInfo(mockContext, null);
		resolver.addProvider(TwProvider.AUTHORITY, provider);

		// Inject a context that knows about our provider
		setContext(mockContext);
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		getContext().getContentResolver().delete(AirportTable.CONTENT_URI,
				null, null);
		getContext().getContentResolver().delete(TripTable.CONTENT_URI, null,
				null);
	}

	private static final String TAG = "TEST";
	
	// Always get resovler this way
	
	
	public void testLoadAirports() throws InterruptedException {
		Intent start = Cmd.SVC_LOAD_AIRPORTS.toStartCmd(getContext());
		startService(start);

		Thread.sleep(10000);

		ContentResolver resolver = getContext().getContentResolver();
		Cursor c = resolver.query(AirportTable.CONTENT_URI, AirportCol.proj(), null, null, null);
		assertTrue(c != null && c.getCount() > 5000);
		
		assertTrue(c.moveToNext());
		String code = c.getString(AirportCol.CODE.ind);
		assertTrue(!TextUtils.isEmpty(code));
		
	}
}

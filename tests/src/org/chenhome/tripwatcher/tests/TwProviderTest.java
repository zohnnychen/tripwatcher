package org.chenhome.tripwatcher.tests;

import org.chenhome.tripwatcher.data.AirportTable;
import org.chenhome.tripwatcher.data.AirportTable.AirportCol;
import org.chenhome.tripwatcher.data.Trip;
import org.chenhome.tripwatcher.data.TripTable;
import org.chenhome.tripwatcher.data.TripTable.TripCol;
import org.chenhome.tripwatcher.data.TwProvider;

import android.annotation.TargetApi;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.test.ProviderTestCase2;
import android.test.mock.MockContentResolver;
import android.text.TextUtils;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class TwProviderTest extends ProviderTestCase2<TwProvider> {

	private MockContentResolver mResolver;

	// @formatter:on
	public TwProviderTest() {
		super(TwProvider.class, TwProvider.AUTHORITY);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mResolver = getMockContentResolver();
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		mResolver.delete(AirportTable.CONTENT_URI, null, null);
		mResolver.delete(TripTable.CONTENT_URI, null, null);
	}

	/**
	 * Test basic functionality with simple, known-good data.
	 */
	public void testAirportCRUD() {

		// Try simple query
		Cursor c = null;
		try {
			ContentValues airport = airport();
			// create
			Uri uri = mResolver.insert(AirportTable.CONTENT_URI, airport);
			assertNotNull(uri);
			assertTrue(ContentUris.parseId(uri) > 0);

			c = mResolver.query(AirportTable.CONTENT_URI, AirportCol.proj(),
					null, null, null);
			assertTrue(c != null && c.moveToNext() && c.getCount() == 1);
			assertEquals(c.getString(AirportCol.NAME.ind), "san fran intl");
			assertEquals(c.getInt(AirportCol.IS_RECENT.ind), 1);
			assertEquals(c.getInt(AirportCol.PROXIMITY_KM.ind), 12);
			assertEquals(c.getDouble(AirportCol.LATITUDE.ind), 36.9645278);

			// update it
			ContentValues c2 = new ContentValues();
			c2.put(AirportCol.PROXIMITY_KM.col, 15);
			assertEquals(1, mResolver.update(uri, c2, null, null));
			c = mResolver.query(uri, AirportCol.proj(), null, null, null);
			assertTrue(c != null && c.moveToNext() && c.getCount() == 1);
			assertEquals(c.getInt(AirportCol.PROXIMITY_KM.ind), 15);

			// delete it
			assertEquals(1, mResolver.delete(uri, null, null));
			c = mResolver.query(uri, AirportCol.proj(), null, null, null);
			assertTrue(c != null && c.getCount() == 0);

		} finally {
			if (c != null)
				c.close();
		}
	}

	/**
	 * Test basic functionality with simple, known-good data.
	 */
	public void testTripCRUD() {

		// Try simple query
		Cursor c = null;
		try {
			ContentValues trip = trip();
			// create
			Uri uri = mResolver.insert(TripTable.CONTENT_URI, trip);
			assertNotNull(uri);
			assertTrue(ContentUris.parseId(uri) > 0);

			c = mResolver.query(TripTable.CONTENT_URI, TripCol.proj(), null,
					null, null);
			assertTrue(c != null && c.moveToNext() && c.getCount() == 1);
			assertEquals(c.getString(TripCol.DST.ind), "SFO");
			assertEquals(c.getInt(TripCol.IS_FLEXIBLE.ind), 1);
			assertEquals(c.getInt(TripCol.PRICE.ind), 300);
			assertEquals(c.getLong(TripCol.UPDATED.ind), 1378267200000L);
			assertEquals(c.getString(TripCol.MONTHS.ind), "1,3,5");

			// update it
			ContentValues c2 = new ContentValues();
			c2.put(TripCol.PRICE.col, 500);
			assertEquals(1, mResolver.update(uri, c2, null, null));
			c = mResolver.query(uri, TripCol.proj(), null, null, null);
			assertTrue(c != null && c.moveToNext() && c.getCount() == 1);
			assertEquals(c.getInt(TripCol.PRICE.ind), 500);

			// delete it
			assertEquals(1, mResolver.delete(uri, null, null));
			c = mResolver.query(uri, TripCol.proj(), null, null, null);
			assertTrue(c != null && c.getCount() == 0);

		} finally {
			if (c != null)
				c.close();
		}
	}

	public void testTripToAndBackFromDb() {
		Trip t = new Trip();
		t.org = "org1";
		t.dst = "dst1";
		t.months = new String[] { "jan", "may" };
		t.timeRanges = new String[] { "om", "oa" };

		ContentValues v = t.toValues();
		assertEquals("org1", v.getAsString(TripCol.ORG.col));
		String months = v.getAsString(TripCol.MONTHS.col);
		String ranges = v.getAsString(TripCol.TIME_RANGES.col);
		assertTrue(!TextUtils.isEmpty(months));
		assertTrue(!TextUtils.isEmpty(ranges));

		// insert it
		Uri uri = mResolver.insert(TripTable.CONTENT_URI, v);
		assertNotNull(uri);

		// get it back
		Cursor c = mResolver.query(uri, TripCol.proj(), null, null, null, null);
		assertTrue(c != null && c.moveToNext());
		Trip t2 = Trip.instantiate(c);
		assertEquals("org1", t2.org);
		assertEquals("dst1", t2.dst);
		assertTrue(t2.months != null && t2.timeRanges != null);
		assertEquals(2, t2.months.length);
		assertEquals(2, t2.timeRanges.length);
		for (String month : t2.months) {
			assertTrue(month.equals("jan") || month.equals("may"));
		}
	}

	public void testTripJson() {
		ContentValues v = trip();
		Uri uri = mResolver.insert(TripTable.CONTENT_URI, v);
		assertNotNull(uri);
		Cursor c = mResolver.query(uri, TripCol.proj(), null, null, null);
		assertTrue(c != null && c.moveToNext());
		Trip t = Trip.instantiate(c);
		String jsonStr = t.toJson();
		assertTrue(!TextUtils.isEmpty(jsonStr));

		Trip t2 = Trip.instantiate(jsonStr);
		assertNotNull(t2.dst);
		assertEquals(t.dst, t2.dst);
		assertEquals(t.price, t2.price);
	}

	private ContentValues airport() {
		ContentValues v = new ContentValues();
		v.put(AirportCol.NAME.col, "san fran intl");
		v.put(AirportCol.CITY.col, "san franicso");
		v.put(AirportCol.CODE.col, "SFO");
		v.put(AirportCol.IS_RECENT.col, 1);
		v.put(AirportCol.IMAGE_URI.col, "file:///assets/foo");
		v.put(AirportCol.LATITUDE.col, 36.9645278);
		v.put(AirportCol.LONGITUDE.col, -86.4196);
		v.put(AirportCol.PROXIMITY_KM.col, 12);
		return v;
	}

	private ContentValues trip() {
		ContentValues v = new ContentValues();
		v.put(TripCol.DEPARTING.col, "2010-05-11");
		v.put(TripCol.DST.col, "SFO");
		v.put(TripCol.IS_FLEXIBLE.col, 1);
		v.put(TripCol.IS_WEEKEND.col, 1);
		v.put(TripCol.MONTHS.col, "1,3,5");
		v.put(TripCol.ORG.col, "SJC");
		v.put(TripCol.PRICE.col, 300);
		v.put(TripCol.PRICE_DELTA.col, 50);
		v.put(TripCol.RETURNING.col, "2010-05-11");
		v.put(TripCol.STOPOVERS.col, 5);
		v.put(TripCol.TIME_RANGES.col, "oa,om,oe");
		v.put(TripCol.UPDATED.col, 1378267200000L);
		return v;
	}
}

package org.chenhome.tripwatcher.tests;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.chenhome.tripwatcher.client.TwApiResp;
import org.chenhome.tripwatcher.data.Flight;
import org.chenhome.tripwatcher.data.Flight.Inbound;
import org.chenhome.tripwatcher.data.Flight.Outbound;

import android.test.InstrumentationTestCase;
import android.text.TextUtils;
import android.util.Log;

public class JsonTest extends InstrumentationTestCase {

	private static final String TAG = "TEST";

	public void testTopDestinations() {
		InputStream stream = getRaw(org.chenhome.tripwatcher.tests.R.raw.flights1);
		InputStreamReader reader = new InputStreamReader(
				new BufferedInputStream(stream));
		verifyResp(reader, 12);
	}

	public void testKnownDestination() {
		InputStream stream = getRaw(org.chenhome.tripwatcher.tests.R.raw.flights2);
		InputStreamReader reader = new InputStreamReader(
				new BufferedInputStream(stream));
		verifyResp(reader, 5);
	}

	public void testKnownDestNDate() {
		InputStream stream = getRaw(org.chenhome.tripwatcher.tests.R.raw.flights3);
		InputStreamReader reader = new InputStreamReader(
				new BufferedInputStream(stream));
		verifyResp(reader, 5);
	}

	
	private void verifyResp(InputStreamReader reader, int results) {
		TwApiResp resp = TwApiResp.fromJson(reader);
		assertNotNull(resp);
		assertEquals(results, resp.resultCount);
		Flight[] flights = resp.flights;
		assertTrue(flights != null && flights.length == results);
		for (Flight f : flights) {
			verify(f);
			assertNotNull(f.inbound);
			assertNotNull(f.outbound);
			verify(f.inbound);
			verify(f.outbound);			
		}
	}

	private void verify(Inbound i) {
		assertNotNull(i.arrival);
		assertNotNull(i.departure);
		assertEquals(113,i.arrival.getYear());
		assertEquals(113,i.departure.getYear());
		assertTrue(!TextUtils.isEmpty(i.carrier));
		assertTrue(i.durationMinutes>=0);
		assertTrue(i.stopoverCount>=0);
		if (i.stopovers!= null && i.stopovers.length>0) {
			for (String over: i.stopovers) {
				assertTrue(!TextUtils.isEmpty(over));
			}
		}
	}

	private void verify(Outbound i) {
		assertNotNull(i.arrival);
		assertNotNull(i.departure);
		assertEquals(113,i.arrival.getYear());
		assertEquals(113,i.departure.getYear());
		assertTrue(!TextUtils.isEmpty(i.carrier));
		assertTrue(i.durationMinutes>=0);
		assertTrue(i.stopoverCount>=0);

		if (i.stopovers != null && i.stopovers.length>0) {
			for (String over: i.stopovers) {
				assertTrue(!TextUtils.isEmpty(over));
			}
		}
	}

	private void verify(Flight f) {
		assertTrue(!TextUtils.isEmpty(f.destination));
		assertTrue(!TextUtils.isEmpty(f.origin));
		assertTrue(!TextUtils.isEmpty(f.link));
		assertTrue(f.price > 0);
	}

	private InputStream getRaw(int resId) {
		return getInstrumentation().getContext().getResources()
				.openRawResource(resId);
	}

}
